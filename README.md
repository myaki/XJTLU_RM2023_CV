# 西交利物浦大学 Robomaster 2023赛季视觉项目
+ 团队: 动云科技 GMaster 战队
+ 成员: 徐亦乐，张昱轩，张煊，廖烽源

## 简介：
本份代码 包含 西交利物浦大学 *RoboMaster 2023* 赛季步兵机器人，英雄机器人的开源代码。本代码主要分为两大类，分别是装甲板识别预测，能量机关识别预测。<br>

```注意``` 
+ 本份代码下所有处理图像的分辨率均为 ```416 x 416```

### 一. 代码运行环境和代码运行设备硬件配置
### 神经网络训练环境： 
#### 硬件环境:
+ 云端训练: AutoDL A4000 * 1 显存：16G
+ 本地训练：AMD 6800XT * 1 显存: 16G
#### 软件环境:
##### 云端训练环境:
+ PyTorch  1.11.0 
+ Python  3.8.10
+ Cuda  11.3
+ OS: Ubuntu 20.04
##### 本地训练环境:
+ PyTorch  1.12.1 
+ Python  3.10.1
+ Rocm 5.2.1
+ OS: Ubuntu 22.04
### 本地台式机模拟运行代码环境:
+ Openvino 22.1
+ CPU: AMD 5600U
+ GPU: \
+ Memory: 8G
+ OS: Ubuntu 20.04
### 赛场运行环境：
#### 硬件环境:
+ 英特尔® NUC 迷你电脑 11 专业版 NUC11TNKv5 (其中内存为 2x 4GB DDR4）
+ 大恒 水星相机 定焦单目相机
#### 软件环境:
+ Ubuntu 20.04 
+ Openvino: 22.1 (GPU推理支持)
+ ros-noetic-desktop-full
+ gcc 9.4.0 cmake 3.16.3 OpenCV 4.6.0
+ Python 3.8.10

### 二. 程序大纲和运行方式
#### 代码命名规范
+ 代码风格: Google (Clion ctrl+alt+l默认的代码风格标准)
+ 函数名: 驼峰命名法
+ 变量名: 驼峰命名法
+ 文件名： _run 函数是一个模块的主函数
#### 程序核心大概大纲
```
.
├── armor  
│   ├── include
│   │   ├── Detector.h  
│   │   ├── ArmorNewYOLO.h
│   │   └── ArmorYOLO.h
│   └── src
│       ├── Detector.cpp //运行主代码
│       ├── ArmorNewYOLO.cpp //装甲板关键点检测模式yolov7
│       └── ArmorYOLO.cpp // 装甲板目标检测模式yolov7
├── camera  //相机调用函数
├── cam_params  //相机相关参数
├── comm   //串口对应函数
├── config //其他相关参数
├── kalman // 卡尔曼滤波模块
│   ├── include
│   │   ├── armor_tracker.h
│   │   ├── robot.h
│   │   ├── robotpredict.h
│   │   ├── Single.h
│   │   ├── robotnow.h //基本废弃
│   │   └── Trajectory.h
│   └── src
│       ├── guardpredict.cpp //装甲板困难版总代码(还没开始写，润！)
│       ├── robot.cpp //装甲板简易版总代码
│       ├── robotnow.cpp //装甲板简易版总代码
│       ├── robotpredict.cpp //装甲板简易版总代码
│       └── Trajectory.cpp //轨迹追踪
├── others 
│   └── GlobalParams.h // 所有的全局常量均在这里
├── particle
│   ├── ParticleFilter.cpp
│   └── ParticleFilter.h//粒子滤波(后期将也会用到装甲板的预测)
├── windmill
│   ├── include
│   │   ├── fan_tracker.h 
│   │   ├── windmill_detect.h
│   │   ├── windmill_predict.h
│   │   ├── windmill_run.h
│   │   └── windmill_trajectory.h
│   └── src
│       ├── fan_tracker.cpp   //扇叶类型整合
│       ├── windmill_detect.cpp // 能量机关识别
│       ├── windmill_predict.cpp // 能量机关轨迹预测
│       ├── windmill_run.cpp   // 能量机关主代码
│       └── windmill_trajectory.cpp //能量机关轨迹追踪
├── .gitignore
├── debug.h  //用来开发可视化调参(目前为雏形)
├── CMakeList.txt
├── main.cpp
└── README.md
```
#### 运行方式
```注意``` 目前仅在 *赛场运行环境* 下测试和运行成功，若选择用Clion打开，则导入CmakeList即可直接运行。若选择命令行编译运行，运行方法如下:
```
mkdir build
cd build
cmake ..
make -j12 //取决于你的cpu核数
./main  //如果你的相机配置需要管理员权限，请添加 sudo 运行
```

### 三. 程序运行架构简要说明
#### 装甲板识别和预测
***这事儿后面说，难绷***

#### 能量机关识别和预测

<br>
<br>

### 四. 效果展示
效果图还没有
<br>
<br>
### 五. 团队成员贡献以及参考文献
#### 团队成团贡献
|成员|负责内容|微信联系方式|
|-|-|-|
| 徐亦乐 | 代码框架耦合，预测算法设计，能量机关拟合<br>ROS开发。| glqjs8923 |
| 张昱轩 | 数据集制作, 神经网络设计，训练，部署以及更新<br> 搭建和配置机器人视觉开发环境 <br> 文档整理，撰写 | zR_ZYX |
| 张煊   | 数据集制作，工程机器人视觉设计。 | ？|
| 廖烽源 | 数据集制作。 | ？|
#### 参考文献链接
+ Yolov7: https://github.com/WongKinYiu/yolov7
+ Yolov5-face: https://github.com/deepcam-cn/yolov5-face
+ yolov5: https://github.com/ultralytics/yolov5
+ 
+ 
