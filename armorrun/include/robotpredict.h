//
// Created by knight on 22-10-23.
//

#ifndef GMASTER_CV_2022_ROBOTPREDICT_H
#define GMASTER_CV_2022_ROBOTPREDICT_H

#include<future>
#include"GlobalParams.h"
#include"ParticleFilter.h"

class robotpredict {
public:
    struct TargetInfo {
        Eigen::Vector3d xyz;
        int dist;
        int timestamp;
    };
    struct PredictStatus {
        bool xyz_status[3];
    };

    robotpredict();

    ~robotpredict();

    bool initParam(string coord_path);

    bool initParam(robotpredict &predictor_loader);

    Eigen::Vector3d predict(Eigen::Vector3d xyz, int timestamp);

    PredictStatus predict_pf_process(TargetInfo target, Eigen::Vector3d &result, int time_estimated);

    PredictStatus predict_fitting_process(Eigen::Vector3d &result, int time_estimated);

    double get_bullet_speed(double speed);

private:

    ParticleFilter pf_pos;                     //目标位置粒子滤波
    ParticleFilter pf_v;                       //速度滤波
    std::deque<TargetInfo> history_info;       //目标队列

    bool fitting_disabled;                                                  //当前是否禁用拟合

    double bullet_speed = 28;

    const int max_timespan = 1000;                                        //最大时间跨度，大于该时间重置预测器(ms)
    const int max_cost = 509;                                            //回归函数最大Cost
    const int max_v = 8;                                                  //设置最大速度,单位m/s
    const int history_deque_len = 12;                                     //队列长度
    const int min_fitting_len = 10;                                         //使用拟合的最短队列长度

public:
    TargetInfo last_target;
    TargetInfo last_pf_target;

    //构建残差
    struct CURVE_FITTING_COST
    {
        CURVE_FITTING_COST (double x, double y) : _x ( x ), _y ( y ) {}
        // 残差的计算
        template <typename T>
        bool operator() (
                const T* const params,     // 模型参数，有3维
                T* residual ) const     // 残差
        {
            residual[0] = T (_y) - params[0] * T(_x) - params[1] * T(_x) * T(_x); // f(x) = a0 + a1 * x + a2 * x^2
            return true;
        }
        const double _x, _y;    // x,y数据
    };


};

#endif //GMASTER_CV_2022_ROBOTPREDICT_H
