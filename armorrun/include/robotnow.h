//
// Created by whoismz on 1/1/22.
//

#ifndef GMASTER_WM_NEW_KALMANFILTER_H
#define GMASTER_WM_NEW_KALMANFILTER_H
//TODO:如果robot区块可以使用，就可以直接去掉
#include "Kalman.h"
#include "../../armor/include/ArmorYOLO.h"
class robotnow
{
public:
    bool predict(RobotCMD& send, cv::Mat& im_show, Robotstatus& getdata);
    Eigen::Vector3d PnP_get_pc(const cv::Point2f p[4], int armor_number);
    void updateMessage(cv::Mat &src,Robotstatus &robotstatus,RobotCMD &robotcmd);
    void velocity_filter(const double &vx_m, const double &vy_m, const double &vz_m);
    void frame_diff();
    struct FramesInfo
    {
        double x;
        double y;
        double z;
        double timestamp;
    };
    std::vector<FramesInfo> frames_info;
    struct ROI
    {
        bool ROI_selected = false;
        cv::Rect2f ROI_bbox;
        int last_class = -1;
        ROI() = default;
        ROI(cv::Rect2f&& bbox, int& last) :ROI_selected(true), ROI_bbox(bbox), last_class(last) {}
        inline void clear()
        {
            ROI_selected = false;
            last_class = -1;
        }
        ~ROI() = default;
    };


    Detection_package package_get;

    robotnow();

    ~robotnow() = default;

    cv::Point2f getCenter(cv::Point2f[4]);

private:

    Eigen::Matrix3d R_CI;           // 陀螺仪坐标系到相机坐标系旋转矩阵EIGEN-Matrix
    Eigen::Matrix3d F;              // 相机内参矩阵EIGEN-Matrix
    Eigen::Matrix<double, 1, 5> C;  // 相机畸变矩阵EIGEN-Matrix
    cv::Mat R_CI_MAT;               // 陀螺仪坐标系到相机坐标系旋转矩阵CV-Mat
    cv::Mat F_MAT;                  // 相机内参矩阵CV-Mat
    cv::Mat C_MAT;                  // 相机畸变矩阵CV-Mat

    YOLO yolo;
    Kalman kalman;

    double test_t = 0;
    double curr_vx, curr_vy, curr_vz;
    ROI roi;

    double last_pitch;
    double last_yaw;

    Trajectory trajectory;

    bbox_t last_armor;

    inline Eigen::Vector3d pc_to_pw(const Eigen::Vector3d &pc, const Eigen::Matrix3d &R_IW) {
        auto R_WC = (R_CI * R_IW).transpose();
        return R_WC * pc;
    }

    inline Eigen::Vector3d pw_to_pc(const Eigen::Vector3d &pw, const Eigen::Matrix3d &R_IW) {
        auto R_CW = R_CI * R_IW;
        return R_CW * pw;
    }

    inline Eigen::Vector3d pc_to_pu(const Eigen::Vector3d &pc) {
        return F * pc / pc(2, 0);
    }

    inline void re_project_point(cv::Mat &image, const Eigen::Vector3d &pw,
                                 const Eigen::Matrix3d &R_IW, const cv::Scalar &color)
    {

        Eigen::Vector3d pc = pw_to_pc(pw, R_IW);
        Eigen::Vector3d pu = pc_to_pu(pc);

//        printf("Af: x: %f y: %f\n\n", pu(0, 0), pu(1, 0));
        cv::circle(image, {int(pu(0, 0)), int(pu(1, 0))}, 4, color, 4);
    }

    inline double getDis(double A, double B)
    {
        return sqrt(A * A + B * B);
    }

    inline double getDis(cv::Point2f A,cv::Point2f B)
    {
        return getDis(B.x-A.x,B.y-A.y);
    }
};

#endif //GMASTER_WM_NEW_KALMANFILTER_H
