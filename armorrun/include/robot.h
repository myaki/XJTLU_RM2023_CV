#include"GlobalParams.h"
#include<chrono>
#include"ArmorYOLO.h"
#include"armor_tracker.h"
#include"Trajectory.h"
#include<future>
#include<vector>
#include"robotpredict.h"
#include"../debug.h"
//#include"ArmorNewYOLO.h"

using namespace std;
using namespace cv;

class robot {
public:
    struct PnPInfo {
        Eigen::Vector3d armor_cam;
        Eigen::Vector3d armor_world;
        Eigen::Vector3d R_cam;
        Eigen::Vector3d R_world;
        Eigen::Vector3d euler;
        Eigen::Matrix3d rmat;
    };

    robot();

    int chooseTarget(const vector<Armor> &armors,int timestamp);

    void updateSpinScore();

    Point2i cropImgByROI(Mat &img);

    bool checkArmor(Armor armor,float radio);

    ArmorTracker* chooseTargetTracker(vector<ArmorTracker*> trackers,int timestamp);

    void load_param(string coord_path, string param_name);

    bool run(RobotCMD &send, cv::Mat &im_show, Robotstatus &getdata);

    robot::PnPInfo PnP(std::vector<Point2f> &p, int armor_number, int method, const Eigen::Matrix3d &R_IW,TargetType type);

    void updateMessage(cv::Mat &src, Robotstatus &robotstatus, RobotCMD &robotcmd);
    //接收包
    Robotstatus robotstatus;
    //发送包
    RobotCMD robotcmd;
    //
    YOLO yolo;

    Trajectory trajectory;
    //TODO:完整写下来之后会删掉这个玩意儿
    Detection_package package_get;

    double last_target_area;

    Point2i roi_offset;

    Point2i last_roi_center;

    Size2i input_size;

    int prev_timestamp;//上一帧时间戳

    std::vector<ArmorTracker> trackers;//装甲板追踪器

    std::vector<Armor> last_armors;//上一组Armor组

    Armor last_armor;//最终确定下来的Last_Armor

    Eigen::Vector3d last_aiming_point;

    std::multimap<string,ArmorTracker> trackers_map;

    std::map<string,int> new_armors_cnt_map;//装甲板计数map，记录新增装甲板数

    std::map<string,int> last_swtiched_time;//装甲板计数map，记录新增装甲板数

    std::map<string,SpinHeading> spin_status_map;//记录在画面里的各个车的状态

    std::map<string,double> spin_rotate_point;//记录敌方小陀螺的可能性分数

    robotpredict predictor;

    robotpredict predictor_param_loader;

private:
    Matrix44d R_CI;
    Matrix44d R_IC;
    Matrix31d T_IW;
    Matrix33d F;
    Matrix15d C;
    Mat R_CI_MAT;
    Mat F_MAT;
    Mat C_MAT;
    //丢失目标的帧数
    int lost_cnt;

    bool is_last_target_exists;

    bool is_target_switched;

    inline Eigen::Vector3d pc_to_pw(const Eigen::Vector3d &pc, const Eigen::Matrix3d &R_IW) {
        Eigen::Vector4d point_camera_tmp;
        Eigen::Vector4d point_imu_tmp;
        Eigen::Vector3d point_imu;
        Eigen::Vector3d point_world;
        Eigen::Vector3d pw_new;

        point_camera_tmp << pc[0], pc[1], pc[2], 1;
        point_imu_tmp = R_IC * point_camera_tmp;
        point_imu << point_camera_tmp[0], point_camera_tmp[1], point_camera_tmp[2];
        point_imu -= T_IW;
        Eigen::Vector3d pw = R_IW * point_imu;
        pw_new << pw(0, 0), pw(2, 0), -pw(1, 0);
        return pw;
    }

    inline Eigen::Vector3d pw_to_pc(const Eigen::Vector3d &pw, const Eigen::Matrix3d &R_IW) {
        Eigen::Vector4d point_camera_tmp;
        Eigen::Vector4d point_imu_tmp;
        Eigen::Vector3d point_imu;
        Eigen::Vector3d point_camera;
        Eigen::Vector3d pw_new;

        //cout<<pw.transpose()<<endl;
        pw_new << pw(0, 0), -pw(2, 0), pw(1, 0);
        //cout<<pw_new.transpose()<<endl;
        point_imu = R_IW.transpose() * pw;
        point_imu += T_IW;
        point_imu_tmp << point_imu[0], point_imu[1], point_imu[2], 1;
        point_camera_tmp = R_CI * point_imu_tmp;
        point_camera << point_camera_tmp[0], point_camera_tmp[1], point_camera_tmp[2];
        return point_camera;
    }
    //工具函数
    inline Eigen::Vector3d pc_to_pu(const Eigen::Vector3d &pc) {
        return F * pc / pc(2, 0);
    }

    inline double getDis(double A, double B) {
        return sqrt(A * A + B * B);
    }

    inline double getDis(cv::Point2f A, cv::Point2f B) {
        return getDis(B.x - A.x, B.y - A.y);
    }

    inline Matrix31d rotationMatrixToEulerAngles(Matrix33d &R) {
        double sy = sqrt(R(0, 0) * R(0, 0) + R(1, 0) * R(1, 0));
        bool singular = sy < 1e-6;
        double x, y, z;
        if (!singular) {
            x = atan2(R(2, 1), R(2, 2));
            y = atan2(-R(2, 0), sy);
            z = atan2(R(1, 0), R(0, 0));
        } else {
            x = atan2(-R(1, 2), R(1, 1));
            y = atan2(-R(2, 0), sy);
            z = 0;
        }
        return {z, y, x};
    }
};