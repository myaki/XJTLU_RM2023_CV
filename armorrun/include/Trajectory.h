#ifndef GMASTER_CV_2022_TRAJECTORY_H
#define GMASTER_CV_2022_TRAJECTORY_H

#include "../../others/GlobalParams.h"

class Trajectory {
public:
    int max_iter;

    float stop_error;

    int R_K_iter;

    Trajectory();

    ~Trajectory() = default;

    double TrajectoryCal(Matrix31d &xyz);

    double getSpeed(double speed);

    void get_iter(int Max_iter,float Stop_error,float R_K_Iter);


private:
    float g = 9.786;

    float k = 0.0196;

    float bullet_speed = 15.6;


};

#endif //GMASTER_CV_2022_TRAJECTORY_H
