//
// Created by whoismz on 1/1/22.
//

#include "../include/robotnow.h"


robotnow::robotnow()
{

    /* 29
    F_MAT = (cv::Mat_<double>(3, 3)
            << 1289.549030, 0.0000000000000000, 327.961334,
            0.0000000000000000, 1289.554962, 255.638963,
            0.0, 0.0, 1.0000000000000000);

    // k1 k2 p1 p2 k3
    C_MAT = (cv::Mat_<double>(1, 5)
            << -0.033200,
            -3.490161,
            0.0000000000000000,
            0.0000000000000000,
            87.551252);
    */

    // 30
    F_MAT = (cv::Mat_<double>(3, 3)
            << 1364.533879, 0.000000, 324.296573,
            0.000000, 1364.747010, 255.291946,
            0.000000, 0.000000, 1.000000);

    // k1 k2 p1 p2 k3
    C_MAT = (cv::Mat_<double>(1, 5)
            << -0.089238, -0.140301, 0.000000, 0.000000, 4.196557);
    R_CI_MAT = (cv::Mat_<double>(3, 3)
            << -0.12264121, 0.9921558, -0.02420736,
            -0.02062435, -0.0269341, -0.99942443,
            -0.99223675, -0.12207136, 0.0237658);


    cv::cv2eigen(R_CI_MAT, R_CI);
    cv::cv2eigen(F_MAT, F);
    cv::cv2eigen(C_MAT, C);

    Kalman::Matrix_61d X;
    Kalman::Matrix_66d A;
    Kalman::Matrix_66d P;
    Kalman::Matrix_66d Q;
    Kalman::Matrix_63d K;
    Kalman::Matrix_36d H;
    Kalman::Matrix_33d R;

    X << 0, 0, 0, 0, 0, 0;
    A <<
      1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0,
            0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 1;

    H <<
      1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0;

    Q <<
      1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0,
            0, 0, 0, 10, 0, 0,
            0, 0, 0, 0, 10, 0,
            0, 0, 0, 0, 0, 10;

    R << 1, 0, 0, 0, 1, 0, 0, 0, 1;

    kalman = Kalman(A, H, Q, R, X, 0);

    last_pitch = 0;
    last_yaw = 0;

    last_armor.confident = 0;
}

cv::Point2f robotnow::getCenter(cv::Point2f pts[4])
{
    for (int i = 0; i < 4; ++i)
    {
        for (int j = i + 1; j < 4; ++j)
        {
            if (pts[i] == pts[j])
            {
                std::cout << "[Error] Unable to calculate center_R point." << std::endl;
                return cv::Point2f{0, 0};
            }
        }
    }
    cv::Point2f center(0, 0);
    if (pts[0].x == pts[2].x && pts[1].x == pts[3].x) {
        std::cout << "[Error] Unable to calculate center_R point." << std::endl;
    } else if (pts[0].x == pts[2].x && pts[1].x != pts[3].x) {
        center.x = pts[0].x;
        center.y = (pts[3].y - pts[1].y) / (pts[3].x - pts[1].x) * (pts[0].x - pts[3].x) + pts[3].y;
    } else if (pts[1].x == pts[3].x && pts[0].x != pts[2].x) {
        center.x = pts[1].x;
        center.y = (pts[2].y - pts[0].y) / (pts[2].x - pts[0].x) * (pts[1].x - pts[0].x) + pts[0].y;
    } else {
        center.x = (((pts[3].y - pts[1].y) / (pts[3].x - pts[1].x) * pts[3].x - pts[3].y + \
                    pts[0].y - (pts[2].y - pts[0].y) / (pts[2].x - pts[0].x) * pts[0].x)) / \
                    ((pts[3].y - pts[1].y) / (pts[3].x - pts[1].x) - (pts[2].y - pts[0].y) / (pts[2].x - pts[0].x));
        center.y = (pts[2].y - pts[0].y) / (pts[2].x - pts[0].x) * (center.x - pts[0].x) + pts[0].y;
    }
    return center;
}

bool robotnow::predict(RobotCMD& send, cv::Mat& im_show, Robotstatus& getdata)
{
    yolo.objects.clear();
    //start for detection
    if(!yolo.workYOLO(im_show))
    {
        cout<<"No object"<<endl;
        return false;
    }
    updateMessage(im_show,getdata,send);
    auto& [detections, img, q_, t0] = package_get;
    im_show = img.clone();

    Eigen::Quaternionf q_raw(q_[0], q_[1], q_[2], q_[3]);
    Eigen::Quaternionf q(q_raw.matrix().transpose());

    Matrix33d R_IW = q.matrix().cast<double>();

    auto robot_status = getdata;

    std::vector<bbox_t> new_detections;

//    cout<<detections.size()<<endl;
//    for(int i=0;i<detections.size();i++)
    //  {
    //      cout<<detections[i].ID<<endl;
    //       cout<<detections[i].color<<endl;
    //       cout<<detections[i].confident;
    //  }
    //  cout<<(int)robot_status.color<<endl;
    for (auto& d : detections)
    {
        cout<<(int)robot_status.color<<" "<<d.color<<endl;
        if ((int)robot_status.color != d.color && d.confident >= 0.5f && d.ID != 0 && d.ID != 2 && d.ID != 6)
        {
            new_detections.push_back(d);
        }
    }
    detections.clear();
    if (new_detections.empty())
    {
        cout<<"Cannot Shoot"<<endl;
        roi.clear();
        return false;
    }

    cout<<"Yes object"<<endl;

    std::sort(detections.begin(), detections.end(), [](const auto& bx, const auto& by)
    {
        auto K1_x = bx.pts[0].x * bx.pts[1].y - bx.pts[1].x * bx.pts[0].y;
        auto K2_x = bx.pts[1].x * bx.pts[2].y - bx.pts[2].x * bx.pts[1].y;
        auto K3_x = bx.pts[2].x * bx.pts[3].y - bx.pts[3].x * bx.pts[2].y;
        auto K4_x = bx.pts[3].x * bx.pts[0].y - bx.pts[0].x * bx.pts[3].y;
        auto S_1 = 0.5 * fabs(K1_x + K2_x + K3_x + K4_x);
        auto K1_y = by.pts[0].x * bx.pts[1].y - by.pts[1].x * bx.pts[0].y;
        auto K2_y = by.pts[1].x * bx.pts[2].y - by.pts[2].x * bx.pts[1].y;
        auto K3_y = by.pts[2].x * bx.pts[3].y - by.pts[3].x * bx.pts[2].y;
        auto K4_y = by.pts[3].x * bx.pts[0].y - by.pts[0].x * bx.pts[3].y;
        auto S_2 = 0.5 * fabs(K1_y + K2_y + K3_y + K4_y);

        return S_1 > S_2;
    });


    auto& armor = new_detections.front();

    cv::Point2f Target = cv::Point2f(288.0,110.0);

    float MIN = 9999;
    for(auto &d : new_detections)
    {
        if(getDis((d.rect.tl()+d.rect.br())*0.5,Target) < MIN)
        {
            armor = d;
            MIN = getDis((d.rect.tl()+d.rect.br())*0.5,Target);
        }
    }
    //cout<<armor.rect.area()<<endl;
    last_armor = armor;
    cout<<(armor.rect.tl()+armor.rect.br())*0.5<<endl;
    cv::circle(im_show, (armor.rect.tl()+armor.rect.br())*0.5, 3, { 255,255,0 });
    //cout<<(armor.rect.tl()+armor.rect.br())*0.5<<endl;
    Matrix31d m_pc = PnP_get_pc(armor.pts, armor.ID);
    Matrix31d m_pw = m_pc;
    //cout<<m_pc(0,0)<<" "<<m_pc(1,0)<<" "<<m_pc(2,0)<<endl;

    Kalman::Matrix_61d X_k = kalman.update(m_pw, 0.2);

    frames_info.push_back({X_k[0], X_k[1], X_k[2], 0.2});
    kalman.last_vx = curr_vx;
    kalman.last_vy = curr_vy;
    kalman.last_vz = curr_vz;
    frame_diff();

    velocity_filter(0.3, 0.3, 0.3);

    double time_p = X_k.norm() / (15.8 * 100);
    Eigen::Vector3d armor_pts_p_w = {
            X_k[0] + curr_vx * time_p,
            X_k[1] + curr_vy * time_p,
            X_k[2] + curr_vz * time_p
    };
    float get_pitch;
    //cout<<s_pw<<endl;

    Matrix31d s_pw_tmp = armor_pts_p_w;
    Matrix31d s_pc = s_pw_tmp;

    Eigen::Vector3d pu = pc_to_pu(s_pc);

//        printf("Af: x: %f y: %f\n\n", pu(0, 0), pu(1, 0));
    cv::circle(im_show, {int(pu(0, 0)), int(pu(1, 0))}, 4, { 0,255,0 }, 4);
//    if(DEBUG)
    cv::circle(im_show, { im_show.cols / 2,im_show.rows / 2 }, 3, { 0,255,255 });
    //re_project_point(im_show, s_pw_tmp, R_IW, { 0,0,255 });
    cv::imshow("ji",im_show);
    cv::waitKey(1);

    //re_project_point(im_show, m_pw, R_IW, { 0,255,0 });
    //re_project_point(im_show, s_pw, R_IW, { 255,0,0 });
    //cv::waitKey(1);

    double s_yaw = atan(-s_pc(0, 0) / s_pc(2, 0)) ;
    double s_pitch = atan(-s_pc(1, 0) / s_pc(2, 0)) ;

    //cout<<getdata.yaw<<" "<<getdata.pitch<<endl;
    send.yaw_angle = (float)(s_yaw);
    send.pitch_angle = (float)(s_pitch)+trajectory.TrajectoryCal(s_pc);

    //cout<<s_yaw<<endl;
    //send

    if (std::abs(send.yaw_angle) < 90 && std::abs(send.pitch_angle) < 90)
        send.shoot_mode = static_cast<uint8_t>(ShootMode::COMMON);
    else
        send.shoot_mode = static_cast<uint8_t>(ShootMode::CRUISE);

    send.priority = Priority::DANGER;

    send.target_id = static_cast<uint8_t>(armor.ID);

    return true;
}

void robotnow::frame_diff()
{
    double dt;
    unsigned long frames_size = frames_info.size();

    int frames_cnt = 30;

    if (frames_size < 5) {
//        curr_vx = 0;
//        curr_vy = 0;
//        curr_vz = 0;
        return;
    }

    if (frames_size < frames_cnt + 1)
    {
        dt = frames_info.back().timestamp - frames_info.front().timestamp;
        curr_vx = (frames_info.back().x - frames_info.front().x) * 100000.0 / ((int) frames_size - 1);
        curr_vy = (frames_info.back().y - frames_info.front().y) * 100000.0 / ((int) frames_size - 1);
        curr_vz = (frames_info.back().z - frames_info.front().z) * 100000.0 / ((int) frames_size - 1);
//        printf("dt: %f\n", dt);
//        printf("%f\n", frames_info.back().x - frames_info.front().x);
//        printf("%f\n", frames_info.back().y - frames_info.front().y);
//        printf("%f\n", frames_info.back().z - frames_info.front().z);
    }
    else
    {
        dt = frames_info[frames_size - 1].timestamp - frames_info[frames_size - frames_cnt - 1].timestamp;
        curr_vx = (frames_info.back().x - frames_info[frames_size - frames_cnt - 1].x) * 100000.0 / frames_cnt;
        curr_vy = (frames_info.back().y - frames_info[frames_size - frames_cnt - 1].y) * 100000.0 / frames_cnt;
        curr_vz = (frames_info.back().z - frames_info[frames_size - frames_cnt - 1].z) * 100000.0 / frames_cnt;
//        printf("dt: %f\n", dt);
//        printf("%f\n", frames_info.back().x - frames_info[frames_size - frames_cnt - 1].x);
//        printf("%f\n", frames_info.back().y - frames_info[frames_size - frames_cnt - 1].y);
//        printf("%f\n", frames_info.back().z - frames_info[frames_size - frames_cnt - 1].z);
    }
}

Eigen::Vector3d robotnow::PnP_get_pc(const cv::Point2f p[4], int armor_number)
{
    static const std::vector<cv::Point3d> pw_small =
            {
                    {-armor_small_l , -armor_small_w, 0},
                    {-armor_small_l , armor_small_w, 0},
                    {armor_small_l , -armor_small_w, 0},
                    {armor_small_l , armor_small_w, 0}
            };

    static const std::vector<cv::Point3d> pw_big =
            {
                    {-armor_big_l,-armor_big_w,0},
                    {-armor_big_l,armor_big_w,0},
                    {armor_big_l,-armor_big_w,0},
                    {armor_big_l,armor_big_w,0}
            };

    std::vector<cv::Point2d> pu(p, p + 4);

    cv::Mat rvec, tvec;
    //solvePnP
    if (armor_number == 0 || armor_number == 1)
    {
        cv::solvePnP(pw_big, pu, F_MAT, C_MAT, rvec, tvec,false,cv::SOLVEPNP_AP3P );
    }

    else
        cv::solvePnP(pw_small, pu, F_MAT, C_MAT, rvec, tvec,false,cv::SOLVEPNP_AP3P);

    Eigen::Vector3d pc;
    cv::cv2eigen(tvec, pc);

   pc << pc(0, 0) + sol_pc_x, pc(1, 0) + sol_pc_y, pc(2, 0) + sol_pc_z;

    return pc;
}

void robotnow::velocity_filter(const double &vx_m, const double &vy_m, const double &vz_m)
{
    if (curr_vx > vx_m) {
        curr_vx = vx_m;
    } else if (curr_vx < -vx_m) {
        curr_vx = -vx_m;
    }
    if (curr_vy > vy_m) {
        curr_vy = vy_m;
    } else if (curr_vy < -vy_m) {
        curr_vy = -vy_m;
    }
    if (curr_vz > vz_m) {
        curr_vz = vz_m;
    } else if (curr_vz < -vz_m) {
        curr_vz = -vz_m;
    }
}

void robotnow::updateMessage(cv::Mat &src, Robotstatus &robotstatus, RobotCMD &robotcmd)
{
    for(int i=0;i<yolo.objects.size();i++)
    {
        bbox_t bbox;
        bbox.confident = yolo.objects[i].prob;
        bbox.rect = yolo.objects[i].rect;
        if(yolo.objects[i].label == 0)
        {
            bbox.color = 1;
            bbox.ID = 1;
        }
        if(yolo.objects[i].label == 1)
        {
            bbox.color = 1;
            bbox.ID = 2;
        }
        if(yolo.objects[i].label == 2)
        {
            bbox.color = 1;
            bbox.ID = 3;
        }
        if(yolo.objects[i].label == 3)
        {
            bbox.color = 1;
            bbox.ID = 4;
        }
        if(yolo.objects[i].label == 4)
        {
            bbox.color = 1;
            bbox.ID = 5;
        }
        if(yolo.objects[i].label == 5)
        {
            bbox.color = 1;
            bbox.ID = 6;
        }
        if(yolo.objects[i].label == 6)
        {
            bbox.color = 1;
            bbox.ID = 0;
        }
        if(yolo.objects[i].label == 7)
        {
            bbox.color = 0;
            bbox.ID = 1;
        }
        if(yolo.objects[i].label == 8)
        {
            bbox.color = 0;
            bbox.ID = 2;
        }
        if(yolo.objects[i].label == 9)
        {
            bbox.color = 0;
            bbox.ID = 3;
        }
        if(yolo.objects[i].label == 10)
        {
            bbox.color = 0;
            bbox.ID = 4;
        }
        if(yolo.objects[i].label == 11)
        {
            bbox.color = 0;
            bbox.ID = 5;
        }
        if(yolo.objects[i].label == 12)
        {
            bbox.color = 0;
            bbox.ID = 6;
        }
        if(yolo.objects[i].label == 13)
        {
            bbox.color = 0;
            bbox.ID = 0;
        }
        bbox.pts[0] = bbox.rect.tl();
        bbox.pts[1] = bbox.rect.tl() + cv::Point(bbox.rect.width,0);
        bbox.pts[2] = bbox.rect.br();
        bbox.pts[3] = bbox.rect.br() + cv::Point(-bbox.rect.width,0);
        package_get.detection.push_back(bbox);

    }
    package_get.q = robotstatus.q;
    robotcmd.pitch_angle = 0;
    robotcmd.yaw_angle = 0;
    package_get.img = src;
    package_get.timestamp = robotstatus.timestamp;
}