#include"robot.h"

bool Areacmp(const YOLO::Object &a, const YOLO::Object &b) {
    return a.rect.area() > b.rect.area();
}

robot::robot() {
    //路径
    string path = "../config/config.yaml";
    string path_predict = "../config/predict.yaml";
    //目前只有test可用
    string param_name = "test";//now
    //相应初始化
    predictor_param_loader.initParam(path_predict);
    predictor.initParam(predictor_param_loader);
    load_param(path, param_name);
    input_size = {640, 384};
    lost_cnt = 0;
}

///---------------------甄别大装甲板-----------------------
bool robot::checkArmor(Armor armor, float radio) {
    if (armor.id == 0 || armor.id == 7)
        return true;
    else if ((armor.id >= 2 && armor.id <= 4) && (armor.id >= 9 && armor.id <= 11) && radio >= armor_radio_threshold)
        return true;
    return false;
}

/**
 * @brief 更新陀螺分数
 * @return void
 */
void robot::updateSpinScore() {
    for (auto score = spin_rotate_point.begin(); score != spin_rotate_point.end();) {
        SpinHeading spin_status;
        //如果压根不存在就是UNKNOWN
        if (spin_rotate_point.count((*score).first) == 0) {
            spin_status = SpinHeading::UNKNOWN;
        } else {
            spin_status = spin_status_map[(*score).first];
        }
        if (abs((*score).second) <= anti_spin_judge_low_thres && spin_status != UNKNOWN) {
            spin_status_map.erase((*score).first);
            score = spin_rotate_point.erase(score);
            continue;
        }
        if (spin_status != UNKNOWN)
            (*score).second = 0.978 * (*score).second - 1 * abs((*score).second) / (*score).second;
        else
            (*score).second = 0.997 * (*score).second - 1 * abs((*score).second) / (*score).second;
        //当小于该值的时候移除该元素
        if (abs((*score).second) < 3 || isnan(abs((*score).second))) {
            spin_status_map.erase((*score).first);
            score = spin_rotate_point.erase(score);
            continue;
        } else if (abs((*score).second) >= anti_spin_judge_large_thres) {
            (*score).second = anti_spin_judge_large_thres * abs((*score).second) / (*score).second;
            if ((*score).second > 0)
                spin_status_map[(*score).first] = SpinHeading::CLOCKWISE;
            if ((*score).second < 0)
                spin_status_map[(*score).first] = SpinHeading::COUNTER_CLOCKWISE;
        }
        ++score;
    }
}

/**
 * @brief 选择目标跟踪器
 * @param trackers
 * @param timestamp
 * @return ArmorTracker
*/
ArmorTracker *robot::chooseTargetTracker(vector<ArmorTracker *> trackers, int timestamp) {
    float max_score = 0;
    int target_idx = 0;
    int last_target_idx = -1;
    for (int i = 0; i < trackers.size(); i++) {
        if (trackers[i]->last_timestamp == timestamp) {
            if (trackers[i]->last_selected_timestamp == prev_timestamp && abs(prev_timestamp - timestamp) < 100)
                last_target_idx = i;
            if (trackers[i]->hit_score > max_score) {
                max_score = trackers[i]->hit_score;
                target_idx = i;
            }
        }
    }
    //若存在上次存在目标且分数相差不大，选择该装甲板
    if (last_target_idx != -1 && abs(trackers[last_target_idx]->hit_score - max_score) / max_score < 0.1)
        target_idx = last_target_idx;
    return trackers[target_idx];
}

Point2i robot::cropImgByROI(Mat &img) {
    // cout<<"lost:"<<lost_cnt<<endl;
    //若上次不存在目标

    auto area_ratio = last_target_area / img.size().area();
    if (!is_last_target_exists) {
        // cout<<lost_cnt<<endl;
        //当丢失目标帧数过多或lost_cnt为初值
        if (lost_cnt > max_lost_cnt || lost_cnt == 0 || area_ratio == 0) {
            return Point2i(0, 0);
        }
    }
    //若目标大小大于阈值
    //丢失目标帧数较小则放大ROI区域
    // if(lost_cnt <= max_lost_cnt)
    // area_ratio*=(1+lost_cnt);

    if (area_ratio > no_crop_ratio) {
        return Point2i(0, 0);
    }
    int max_expand = (img.size().height - input_size.height) / 2;
    int expand_value = (int) ((area_ratio / no_crop_ratio) * max_expand) / 32 * 32;
    // cout<<"last:"<<last_roi_center<<endl;
    // Size2i cropped_size = input_size;
    Size2i cropped_size = {input_size + Size2i(expand_value, expand_value)};
    // cout<<cropped_size<<" "<<expand_value<<endl;
    if (cropped_size.width >= img.size().width)
        cropped_size.width = img.size().width;
    if (cropped_size.height >= img.size().height)
        cropped_size.height = img.size().height;

    //处理X越界
    if (last_roi_center.x <= cropped_size.width / 2)
        last_roi_center.x = cropped_size.width / 2;
    if (last_roi_center.x > (img.size().width - cropped_size.width / 2))
        last_roi_center.x = img.size().width - cropped_size.width / 2;
    //处理Y越界
    if (last_roi_center.y <= cropped_size.height / 2)
        last_roi_center.y = cropped_size.height / 2;
    if (last_roi_center.y > (img.size().height - cropped_size.height / 2))
        last_roi_center.y = img.size().height - cropped_size.height / 2;

    //左上角顶点
    auto offset = last_roi_center - Point2i(cropped_size.width / 2, cropped_size.height / 2);
    // auto offset = last_roi_center - Point2i(roi_width / 2, roi_height / 2);
    Rect roi_rect = Rect(offset, cropped_size);
    img(roi_rect).copyTo(img);

    // namedWindow("roi", img);
    return offset;
}

/**
 * @brief 目标选择
 * @param armors
 * @param timestamp
 * @return int
*/
int robot::chooseTarget(const vector<Armor> &armors, int timestamp) {
    //常规筛选应该以英雄为优先,然后是降级到连续跟到的目标，最后是多个目标中面积最大的
    //TODO:筛选的可能优化
    bool is_last_id_exists = false;
    int target_id;
    for (auto armor: armors) {
        if (armor.id == 1 && armor.center3d_cam.norm() <= hero_danger_zone) {
            return armor.id;
        } else if (armor.id == last_armor.id && abs(armor.area - last_armor.area) / (float) armor.area < 0.3 &&
                   abs(timestamp - prev_timestamp) < 30) {
            is_last_id_exists = true;
            target_id = armor.id;
        }
    }
    if (is_last_id_exists == true)
        return target_id;
    return (*armors.begin()).id;
}

/**
 * @brief 参数导入
 * @param coord_path YAML存储路径，param_name为本车信息
*/
void robot::load_param(std::string coord_path, std::string param_name) {
    YAML::Node Config = YAML::LoadFile(coord_path);

    Eigen::MatrixXd mat_xyz_offset(1, 3);
    Eigen::MatrixXd mat_angle_offset(1, 2);
    Eigen::MatrixXd mat_c(1, 5);
    Eigen::MatrixXd mat_f(3, 3);
    Eigen::MatrixXd mat_r_ci(4, 4);
    Eigen::MatrixXd mat_r_ic(4, 4);
    Eigen::MatrixXd mat_t_iw(1, 3);

    trajectory.max_iter = Config[param_name]["max_iter"].as<int>();
    trajectory.stop_error = Config[param_name]["stop_error"].as<float>();
    trajectory.R_K_iter = Config[param_name]["R_K_iter"].as<int>();

    auto read_vector = Config[param_name]["Intrinsic"].as<vector<float>>();
    initMatrix(mat_f, read_vector);
    F = mat_f;
    cv::eigen2cv(F, F_MAT);

    read_vector = Config[param_name]["Coeff"].as<vector<float>>();
    initMatrix(mat_c, read_vector);
    C = mat_c;
    cv::eigen2cv(C, C_MAT);

    read_vector = Config[param_name]["R_CI"].as<vector<float>>();
    initMatrix(mat_r_ci, read_vector);
    //R_CI = mat_r_ci;
    R_CI = Eigen::Matrix4d::Identity();
    cv::eigen2cv(R_CI, R_CI_MAT);

    read_vector = Config[param_name]["R_IC"].as<vector<float>>();
    initMatrix(mat_r_ic, read_vector);
    R_IC = Eigen::Matrix4d::Identity();
    //R_IC = mat_r_ic;

    read_vector = Config[param_name]["T_CI"].as<vector<float>>();
    initMatrix(mat_t_iw, read_vector);
    T_IW = mat_t_iw.transpose();


}

/*!
 * @brief 装甲板自描主函数
 * @param send
 * @param im_show
 * @param getdata
 * @return bool
 */
bool robot::run(RobotCMD &send, cv::Mat &im_show, Robotstatus &getdata) {

    yolo.objects.clear();
    auto t0 = std::chrono::steady_clock::now();
    auto timestamp = getdata.timestamp;

    vector<Armor> armors;
    vector<YOLO::Object> objects;

    roi_offset = cropImgByROI(im_show);
    ///----------------------------装甲板识别-----------------------------
    if (!yolo.workYOLO(im_show)) {
#ifdef SHOW_AIM_CROSS
        line(im_show, Point2f(im_show.size().width / 2, 0), Point2f(im_show.size().width / 2, im_show.size().height), {0,255,0}, 1);
        line(im_show, Point2f(0, im_show.size().height / 2), Point2f(im_show.size().width, im_show.size().height / 2), {0,255,0}, 1);
#endif
#ifdef SHOW_IMG
        namedWindow("dst",0);
        imshow("dst",im_show);
        waitKey(1);
#endif
        updateSpinScore();
        lost_cnt++;
        is_last_target_exists = false;
        return false;
    }

    Eigen::Quaternionf q_raw(getdata.q[0], getdata.q[1], getdata.q[2], getdata.q[3]);
    Eigen::Quaternionf q(q_raw.matrix().transpose());
    Eigen::Matrix3d R_IW = q.matrix().cast<double>();

    sort(objects.begin(), objects.end(), Areacmp);
    //若对象较多保留前按面积排序后的前max_armors个
    if (objects.size() > armor_max_size)
        objects.resize(armor_max_size);

    ///---------------------------装甲版信息处理并存储在Armor---------------------------------
    for (auto object: yolo.objects) {
        Armor armor;
        armor.id = object.label;
        armor.conf = object.prob;
        armor.color = (int) robotstatus.color;
        //TODO:armor.id的问题
        if (armor.color == (int) ColorChoose::BLUE)
            armor.key = "B" + to_string(armor.id+1);
        else
            armor.key = "R" + to_string(armor.id+1);

        memcpy(armor.apex2d, object.point, 4 * sizeof(cv::Point2f));
        for (int i = 0; i < 4; i++) {
            armor.apex2d[i] += Point2f((float) roi_offset.x, (float) roi_offset.y);
        }
        Point2f apex_sum;
        for (auto apex: armor.apex2d)
            apex_sum += apex;
        armor.center2d = apex_sum / 4.f;
        //生成装甲板旋转矩形和ROI
        std::vector<Point2f> points_pic(armor.apex2d, armor.apex2d + 4);
        RotatedRect points_pic_rrect = minAreaRect(points_pic);
        armor.rrect = points_pic_rrect;
        auto bbox = points_pic_rrect.boundingRect();
        auto x = bbox.x - 0.5 * bbox.width * (armor_roi_expand_ratio_width - 1);
        auto y = bbox.y - 0.5 * bbox.height * (armor_roi_expand_ratio_height - 1);
        armor.roi = Rect(x,
                         y,
                         bbox.width * armor_roi_expand_ratio_width,
                         bbox.height * armor_roi_expand_ratio_height
        );
        if (armor.conf < armor_high_threshold_conf) {
            if (last_armors.empty()) {
                continue;
            } else {
                bool is_this_armor_available = false;
                for (auto last_armor: last_armors) {
                    if (last_armor.roi.contains(armor.center2d)) {
                        is_this_armor_available = true;
                        break;
                    }
                }
                if (!is_this_armor_available) {
                    continue;
                }
            }
        }
        auto apex_wh_ratio = max(points_pic_rrect.size.height, points_pic_rrect.size.width) /
                             min(points_pic_rrect.size.height, points_pic_rrect.size.width);
        int pnp_method;
        //装甲板少的时候使用 ITRATIVE, 多的时候使用 IPPE
        if (objects.size() <= 2)
            pnp_method = SOLVEPNP_ITERATIVE;
        else
            pnp_method = SOLVEPNP_IPPE;

        TargetType type;
        if (checkArmor(armor, apex_wh_ratio))
            type = BIG;
        else
            type = SMALL;
        auto pnp_result = PnP(points_pic, armor.id, pnp_method, R_IW, type);
        if (pnp_result.armor_cam.norm() > 13 ||
            isnan(pnp_result.armor_cam[0]) ||
            isnan(pnp_result.armor_cam[1]) ||
            isnan(pnp_result.armor_cam[2]))
            continue;

        armor.type = type;
        armor.center3d_world = pnp_result.armor_world;
        armor.center3d_cam = pnp_result.armor_cam;
        armor.euler = pnp_result.euler;
        armor.area = object.rect.area();
        armors.push_back(armor);
    }

    if (armors.empty()) {
#ifdef SHOW_AIM_CROSS
        line(im_show, Point2f(im_show.size().width / 2, 0), Point2f(im_show.size().width / 2, im_show.size().height), {0,255,0}, 1);
        line(im_show, Point2f(0, im_show.size().height / 2), Point2f(im_show.size().width, im_show.size().height / 2), {0,255,0}, 1);
#endif
#ifdef SHOW_IMG
        namedWindow("dst",0);
        imshow("dst",im_show);
        waitKey(1);
#endif
        updateSpinScore();
        lost_cnt++;
        is_last_target_exists = false;
        return false;
    }
    ///-------------------------------------构建ArmorTracker------------------------------------
    new_armors_cnt_map.clear();
    //为装甲板分配或新建最佳ArmorTracker
    //注:将不会为灰色装甲板创建预测器，只会分配给现有的预测器
    for (auto armor = armors.begin(); armor != armors.end(); ++armor) {
        //当装甲板颜色为灰色且当前dead_buffer小于max_dead_buffer
        string tracker_key;
        if ((*armor).color == 2) {

            if (robotstatus.color == ColorChoose::RED)
                tracker_key = "R" + to_string((*armor).id+1);
            if (robotstatus.color == ColorChoose::BLUE)
                tracker_key = "B" + to_string((*armor).id+1);
        } else {
            tracker_key = (*armor).key;
        }

        auto predictors_with_same_key = trackers_map.count(tracker_key);
        //当不存在该类型装甲板ArmorTracker且该装甲板Tracker类型不为灰色装甲板
        if (predictors_with_same_key == 0 && (*armor).color != 2) {
            ArmorTracker tracker((*armor), timestamp);
            auto target_predictor = trackers_map.insert(make_pair((*armor).key, tracker));
            new_armors_cnt_map[(*armor).key]++;
        }
            //当存在一个该类型ArmorTracker
        else if (predictors_with_same_key == 1) {
            auto candidate = trackers_map.find(tracker_key);
            auto delta_t = timestamp - (*candidate).second.last_timestamp;
            auto delta_dist = ((*armor).center3d_world - (*candidate).second.last_armor.center3d_world).norm();
            // auto iou = (*candidate).second.last_armor.roi & (*armor)
            // auto velocity = (delta_dist / delta_t) * 1e3;
            //若匹配则使用此ArmorTracker
            if (delta_dist <= max_delta_dist && delta_t > 0 &&
                (*candidate).second.last_armor.roi.contains((*armor).center2d)) {
                (*candidate).second.update((*armor), timestamp);
            }
                //若不匹配则创建新ArmorTracker
            else if ((*armor).color != 2) {
                ArmorTracker tracker((*armor), timestamp);
                trackers_map.insert(make_pair((*armor).key, tracker));
                new_armors_cnt_map[(*armor).key]++;
            }
        }
            //当存在多个该类型装甲板ArmorTracker
        else {
            //1e9无实际意义，仅用于以非零初始化
            double min_delta_dist = 1e9;
            int min_delta_t = 1e9;
            bool is_best_candidate_exist = false;
            std::multimap<string, ArmorTracker>::iterator best_candidate;
            auto candiadates = trackers_map.equal_range(tracker_key);
            //遍历所有同Key预测器，匹配速度最小且更新时间最近的ArmorTracker
            for (auto iter = candiadates.first; iter != candiadates.second; ++iter) {
                auto delta_t = timestamp - (*iter).second.last_timestamp;
                auto delta_dist = ((*armor).center3d_world - (*iter).second.last_armor.center3d_world).norm();
                auto velocity = (delta_dist / delta_t) * 1e3;

                if ((*iter).second.last_armor.roi.contains((*armor).center2d) && delta_t > 0) {
                    if (delta_dist <= max_delta_dist && delta_dist <= min_delta_dist &&
                        delta_t <= min_delta_t) {
                        min_delta_t = delta_t;
                        min_delta_dist = delta_dist;
                        best_candidate = iter;
                        is_best_candidate_exist = true;
                    }
                }
            }
            if (is_best_candidate_exist) {
                auto velocity = min_delta_dist;
                auto delta_t = min_delta_t;
                (*best_candidate).second.update((*armor), timestamp);
            } else if ((*armor).color != 2) {
                ArmorTracker tracker((*armor), timestamp);
                trackers_map.insert(make_pair((*armor).key, tracker));
                new_armors_cnt_map[(*armor).key]++;
            }

        }
    }

    if (trackers_map.size() != 0) {
        //维护预测器Map，删除过久之前的装甲板
        for (auto iter = trackers_map.begin(); iter != trackers_map.end();) {
            //删除元素后迭代器会失效，需先行获取下一元素
            auto next = iter;
            // cout<<(*iter).second.last_timestamp<<"  "<<src.timestamp<<endl;
            if ((timestamp - (*iter).second.last_timestamp) > max_delta_t)
                next = trackers_map.erase(iter);
            else
                ++next;
            iter = next;
        }
    }
    ///-------------------------------计算每个车胡陀螺分数------------------------------------
    for (auto cnt: new_armors_cnt_map) {
        //只在该类别新增装甲板时数量为1时计算陀螺分数
        if (cnt.second == 1) {
            auto same_armors_cnt = trackers_map.count(cnt.first);
            if (same_armors_cnt == 2) {
                // cout<<"1"<<endl;
                //遍历所有同Key预测器，确定左右侧的Tracker
                ArmorTracker *new_tracker = nullptr;
                ArmorTracker *last_tracker = nullptr;
                double last_armor_center;
                double last_armor_timestamp;
                double new_armor_center;
                double new_armor_timestamp;
                int best_prev_timestamp = 0;    //候选ArmorTracker的最近时间戳
                auto candiadates = trackers_map.equal_range(cnt.first);
                for (auto iter = candiadates.first; iter != candiadates.second; ++iter) {
                    //若未完成初始化则视为新增tracker
                    if (!(*iter).second.is_initialized && (*iter).second.last_timestamp == timestamp) {
                        new_tracker = &(*iter).second;
                    } else if ((*iter).second.last_timestamp > best_prev_timestamp && (*iter).second.is_initialized) {
                        best_prev_timestamp = (*iter).second.last_timestamp;
                        last_tracker = &(*iter).second;
                    }

                }
                if (new_tracker != nullptr && last_tracker != nullptr) {
                    new_armor_center = new_tracker->last_armor.center2d.x;
                    new_armor_timestamp = new_tracker->last_timestamp;
                    last_armor_center = last_tracker->last_armor.center2d.x;
                    last_armor_timestamp = last_tracker->last_timestamp;
                    auto spin_movement = new_armor_center - last_armor_center;
                    // auto delta_t =
                    LOG(INFO) << "[SpinDetection] Candidate Spin Movement Detected : " << cnt.first << " : "
                              << spin_movement;
                    if (abs(spin_movement) > 10 && new_armor_timestamp == timestamp &&
                        last_armor_timestamp == timestamp) {

                        //若无该元素则插入新元素
                        if (spin_rotate_point.count(cnt.first) == 0) {
                            spin_rotate_point[cnt.first] = 1000 * spin_movement / abs(spin_movement);
                        }
                            //若已有该元素且目前旋转方向与记录不同,则对目前分数进行减半惩罚
                        else if (spin_movement * spin_rotate_point[cnt.first] < 0) {
                            spin_rotate_point[cnt.first] *= 0.5;
                        }
                            //若已有该元素则更新元素
                        else {
                            spin_rotate_point[cnt.first] = anti_spin_max_r_multiple * spin_rotate_point[cnt.first];
                        }
                    }
                }
            }
        }
    }

    updateSpinScore();
    auto target_id = chooseTarget(armors, timestamp);

    string target_key;
    if (robotstatus.color == ColorChoose::BLUE)
        target_key = "B" + to_string(target_id);
    else
        target_key = "R" + to_string(target_id);

    if (trackers_map.count(target_key) == 0) {
#ifdef SHOW_AIM_CROSS
        line(im_show, Point2f(im_show.size().width / 2, 0), Point2f(im_show.size().width / 2, im_show.size().height), {0,255,0}, 1);
        line(im_show, Point2f(0, im_show.size().height / 2), Point2f(im_show.size().width, im_show.size().height / 2), {0,255,0}, 1);
#endif
#ifdef SHOW_IMG
        namedWindow("dst",0);
        imshow("dst",im_show);
        waitKey(1);
#endif
        is_last_target_exists = false;
        lost_cnt++;
        cout << "No trackers" << endl;
        return false;
    }

    auto ID_candiadates = trackers_map.equal_range(target_key);

    bool is_target_spinning;
    Armor target;
    Eigen::Vector3d aiming_point;
    std::vector<ArmorTracker *> final_trackers;
    std::vector<Armor> final_armors;

    //若目标处于陀螺状态，预先瞄准目标中心，待预测值与该点距离较近时开始击打
    SpinHeading spin_status;

    if (spin_status_map.count(target_key) == 0) {
        spin_status = UNKNOWN;
        is_target_spinning = false;
    } else {
        spin_status = spin_status_map[target_key];
        if (spin_status != UNKNOWN)
            is_target_spinning = true;
        else
            is_target_spinning = false;
    }
    ///---------------------------------反陀螺击打----------------------------------------
    if (spin_status != UNKNOWN) {
        auto available_candidates_cnt = 0;
        for (auto iter = ID_candiadates.first; iter != ID_candiadates.second; ++iter) {
            if ((*iter).second.last_timestamp == timestamp) {
                final_armors.push_back((*iter).second.last_armor);
                final_trackers.push_back(&(*iter).second);
            } else {
                continue;
            }
            // // 若Tracker未完成初始化，不考虑使用
            // if (!(*iter).second.is_initialized || (*iter).second.history_info.size() < 3)
            // {
            //     continue;
            // }
            // else
            // {
            //     final_trackers.push_back(&(*iter).second);
            //     available_candidates_cnt++;
            // }
        }
        // if (available_candidates_cnt == 0)
        // {
        //     cout<<"Invalid"<<endl;
        // }
        // else
        // {
        //     //TODO:空间构建是否可行
        //     //-----------------------------计算陀螺旋转半径--------------------------------------
        //     Eigen::Vector3d rotate_center_cam = {0,0,0};
        //     Eigen::Vector3d rotate_center_car = {0,0,0};
        //     for(auto tracker : final_trackers)
        //     {
        //         std::vector<Eigen::Vector3d> pts;
        //         for (auto pt : tracker->history_info)
        //         {
        //             pts.push_back(pt.center3d_world);
        //         }
        //         auto sphere = FitSpaceCircle(pts);
        //         auto radius = sphere[3];
        //         if (tracker->radius == 0)
        //             tracker->radius = radius;
        //         else//若不为初值，尝试进行半径平均以尽量误差
        //             tracker->radius = (tracker->radius + radius) / 2;
        //         //-----------------------------计算陀螺中心与预瞄点-----------------------------------
        //         //此处世界坐标系指装甲板世界坐标系，而非车辆世界坐标系
        //         Eigen::Vector3d rotate_center_world = {0,
        //                             sin(25 * 180 / CV_PI) * tracker->radius,
        //                             - cos(25 * 180 / CV_PI) * tracker->radius};
        //         auto rotMat = eulerToRotationMatrix(tracker->prev_armor.euler);
        //         //Pc = R * Pw + T
        //         rotate_center_cam = (rotMat * rotate_center_world) + tracker->prev_armor.center3d_cam;
        //         rotate_center_car += coordsolver.worldToCam(rotate_center_cam, rmat_imu);
        //     }
        //     //求解旋转中心
        //     rotate_center_car /= final_trackers.size();
        // }
        //若存在一块装甲板
        if (final_armors.size() == 1) {
            target = final_armors.at(0);
        }
            //若存在两块装甲板
        else if (final_armors.size() == 2) {
            //对最终装甲板进行排序，选取与旋转方向相同的装甲板进行更新
            sort(final_armors.begin(), final_armors.end(),
                 [](Armor &prev, Armor &next) { return prev.center3d_cam[0] < next.center3d_cam[0]; });
            //若顺时针旋转选取右侧装甲板更新
            if (spin_status == CLOCKWISE)
                target = final_armors.at(1);
                //若逆时针旋转选取左侧装甲板更新
            else if (spin_status == COUNTER_CLOCKWISE)
                target = final_armors.at(0);
        }

        //判断装甲板是否切换，若切换将变量置1
        auto delta_t = timestamp - prev_timestamp;
        auto delta_dist = (target.center3d_world - last_armor.center3d_world).norm();
        auto velocity = (delta_dist / delta_t) * 1e3;
        if ((target.id != last_armor.id || !last_armor.roi.contains((target.center2d))) &&
            is_last_target_exists)
            is_target_switched = true;
        else
            is_target_switched = false;
#ifdef USING_PREDICT
        if (is_target_switched) {
            predictor.initParam(predictor_param_loader);
            aiming_point = target.center3d_cam;
        } else {
            auto aiming_point_world = predictor.predict(target.center3d_world, timestamp);
            // aiming_point = aiming_point_world;
            aiming_point = pw_to_pc(aiming_point_world, R_IW);
        }
#else
        // aiming_point = pw_to_pc(target.center3d_world,R_IW);
        aiming_point = target.center3d_cam;
#endif //USING_PREDICT
    }
        ///-----------------------------------常规击打------------------------------------
    else {
        for (auto iter = ID_candiadates.first; iter != ID_candiadates.second; ++iter) {
            // final_armors.push_back((*iter).second.last_armor);
            final_trackers.push_back(&(*iter).second);
        }
        //进行目标选择
        auto tracker = chooseTargetTracker(final_trackers, timestamp);
        tracker->last_selected_timestamp = timestamp;
        tracker->selected_cnt++;
        target = tracker->last_armor;
        //判断装甲板是否切换，若切换将变量置1
        auto delta_t = timestamp - prev_timestamp;
        auto delta_dist = (target.center3d_world - last_armor.center3d_world).norm();
        auto velocity = (delta_dist / delta_t) * 1e3;
        // cout<<(delta_dist >= max_delta_dist)<<" "<<!last_armor.roi.contains(target.center2d)<<endl;
        if ((target.id != last_armor.id || !last_armor.roi.contains((target.center2d))) &&
            is_last_target_exists)
            is_target_switched = true;
        else
            is_target_switched = false;


#ifdef USING_PREDICT
        //目前类别预测不是十分稳定,若之后仍有问题，可以考虑去除类别判断条件
        if (is_target_switched) {
            predictor.initParam(predictor_param_loader);
            // cout<<"initing"<<endl;
            aiming_point = target.center3d_cam;
        } else {
            auto aiming_point_world = predictor.predict(target.center3d_world, timestamp);
            // aiming_point = aiming_point_world;
            aiming_point = pw_to_pc(aiming_point_world, R_IW);
        }
#else
        // aiming_point = pw_to_pc(target.center3d_world,R_IW);
        aiming_point = target.center3d_cam;
#endif //USING_PREDICT
    }
    ///------------------------------------------状态更新-------------------------------------------
    last_roi_center = target.center2d;
    // last_roi_center = Point2i(512,640);
    last_armor = target;
    lost_cnt = 0;
    prev_timestamp = timestamp;
    last_target_area = target.area;
    last_aiming_point = aiming_point;
    is_last_target_exists = true;
    last_armors.clear();
    last_armors = armors;

    double s_yaw = atan(-aiming_point(0, 0) / aiming_point(2, 0));
    double s_pitch = atan(-aiming_point(1, 0) / aiming_point(2, 0));

    send.yaw_angle = (float) (s_yaw);
    send.pitch_angle = (float) (s_pitch) + trajectory.TrajectoryCal(aiming_point);
#ifdef SHOW_AIM_CROSS
    line(im_show, Point2f(im_show.size().width / 2, 0), Point2f(im_show.size().width / 2, im_show.size().height), {0,255,0}, 1);
    line(im_show, Point2f(0, im_show.size().height / 2), Point2f(im_show.size().width, im_show.size().height / 2), {0,255,0}, 1);
#endif
#ifdef SHOW_IMG
    namedWindow("dst",0);
        imshow("dst",im_show);
        waitKey(1);
#endif

    cout << send.yaw_angle << endl;
    if (isnan(send.yaw_angle) || isnan(send.pitch_angle)) {
        cout << "NONONONONO" << endl;
        return false;
    }

    return true;
}


/*!
 * @brief 解算装甲板并计算相应坐标
 * @param p
 * @param armor_number
 * @param method
 * @param R_IW
 * @param type
 * @return PnPInfo
 */
robot::PnPInfo
robot::PnP(std::vector<Point2f> &p, int armor_number, int method, const Eigen::Matrix3d &R_IW, TargetType type) {
    PnPInfo result;
    Eigen::Matrix3d rmat_eigen;
    Eigen::Vector3d tvec_eigen;
    static const std::vector<cv::Point3d> pw_small =
            {
                    {-armor_small_l, -armor_small_w, 0},
                    {-armor_small_l, armor_small_w,  0},
                    {armor_small_l,  -armor_small_w, 0},
                    {armor_small_l,  armor_small_w,  0}
            };
    static const std::vector<cv::Point3d> pw_big =
            {
                    {-armor_big_l, -armor_big_w, 0},
                    {-armor_big_l, armor_big_w,  0},
                    {armor_big_l,  -armor_big_w, 0},
                    {armor_big_l,  armor_big_w,  0}
            };
    cv::Mat rvec, tvec;
    //solvePnP
    if (type == TargetType::BIG)
        cv::solvePnP(pw_big, p, F_MAT, C_MAT, rvec, tvec, false, method);
    else
        cv::solvePnP(pw_small, p, F_MAT, C_MAT, rvec, tvec, false, method);

    Mat r_mat;
    Rodrigues(rvec, r_mat);
    Eigen::Vector3d pc;
    cv::cv2eigen(rvec, rmat_eigen);
    cv::cv2eigen(tvec, tvec_eigen);

    result.armor_cam = tvec_eigen;
    result.armor_world = pc_to_pw(result.armor_cam, R_IW);
    result.euler = rotationMatrixToEulerAngles(rmat_eigen);

    return result;
}