#include "armor/include/Detector.h"
#include "camera/include/camera/CamWrapperDH.h"
#include "opencv2/core/core_c.h"

using namespace cv;
using namespace std;
Mat img_src;
Camera *cam = nullptr;
Detector *adetector = nullptr;
CommPort comm;
TickMeter meter;
Robotstatus robotstatus;

bool linkCam_DaHeng();

void withVideo(const std::string &path);

int main(int argc,char **argv)
{
    adetector = new Detector(comm);
    if (FOR_PC)
        withVideo("../test_armor_videos/test.mp4");
    else
        linkCam_DaHeng();
    return 0;
}

void withVideo(const std::string &path)
{
    VideoCapture cap(path);
    while (true) {
        cap.read(img_src);
        if (img_src.empty()) {
            cout << "没有找到视频" << endl;
            break;
        }
        meter.start();
        adetector->run(img_src);
        meter.stop();
        printf("Time: %f\n", meter.getTimeMilli());
        meter.reset();
        imshow("f",img_src);
        waitKey(1);
    }
    cap.release();
}

bool linkCam_DaHeng()
{
    //实际上的用处就是可以控制是否是打符的曝光
    bool flag = false; // tradition
    comm.Start();
    cam = new DHCamera("KN0210060029");
    cam->init(0, 0, 640, 384, 10000, 6, false);
//  cv::VideoWriter writer_1("../record_armor_videos/armor_test_a90.avi", cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 100, cv::Size(640, 384));
    while (true)
    {
        if ((robotstatus.vision == Vision::WINDSMALL || robotstatus.vision == Vision::WINDBIG) && flag == false)
        {
            flag = true;
            cam->init(0, 0, 640, 384, 10000, 6, false);
        }
        else if ((robotstatus.vision == Vision::CLASSIC || robotstatus.vision == Vision::SENTLY) && flag == true)
        {
            flag = false;
            cam->init(0, 0, 640, 384, 10000, 6, false);
        }
        if (!cam->start())
            return false;
        cam->read(img_src);
        if (img_src.empty())
            break;
        //writer_1 << img_src;
        meter.start(); //计时器
        adetector->run(img_src);
        meter.stop(); //停止计时
        printf("Time1: %f\n", meter.getTimeMilli()); //打印时长
        meter.reset();
        imshow(" ",img_src);
        waitKey(1);
//        writer_2 << img_src;
    }
    return true;
}




