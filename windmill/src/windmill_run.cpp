#include "windmill_run.h"

BUFF::BUFF() {
    detector.initModel(network_path);
    lost_cnt = 0;
    is_last_target_exists = false;
    input_size = {640, 480};
    last_bullet_speed = 0;
    cnt = 0;
    right_cnt = 0;
    left_cnt = 0;
    is_Start = false;
    sign = 0;
    final_sign = 0;

    //fmt::print(fmt::fg(fmt::color::pale_violet_red), "[BUFF] Buff init model success! Size: {} {}\n", input_size.height, input_size.width);
}

BUFF::~BUFF() = default;

#ifdef USING_ROI

Point2i BUFF::cropImageByROI(Mat &img) {
    if (!is_last_target_exists) {
        //当丢失目标帧数过多或lost_cnt为初值
        if (lost_cnt > max_lost_cnt || lost_cnt == 0) {
            return Point2i(0, 0);
        }
    }
    //若目标大小大于阈值
    // cout<<last_target_area / img.size().area()<<endl;
    if ((last_target_area / img.size().area()) > no_crop_thres) {
        return Point2i(0, 0);
    }
    //处理X越界
    if (last_roi_center.x <= input_size.width / 2)
        last_roi_center.x = input_size.width / 2;
    else if (last_roi_center.x > (img.size().width - input_size.width / 2))
        last_roi_center.x = img.size().width - input_size.width / 2;
    //处理Y越界
    if (last_roi_center.y <= input_size.height / 2)
        last_roi_center.y = input_size.height / 2;
    else if (last_roi_center.y > (img.size().height - input_size.height / 2))
        last_roi_center.y = img.size().height - input_size.height / 2;

    //左上角顶点
    auto offset = last_roi_center - Point2i(input_size.width / 2, input_size.height / 2);
    Rect roi_rect = Rect(offset, input_size);
    img(roi_rect).copyTo(img);

    return offset;
}

#endif

//目标选择函数
bool BUFF::chooseTarget(vector<Fan> &fans, Fan &target) {
    float max_area = 0;
    int target_idx = 0;
    int target_fan_cnt = 0;
    for (auto fan: fans) {
        if (fan.id == 1) {
            target = fan;
            target_fan_cnt++;
        }
    }
    if (target_fan_cnt == 1)
        return true;
    else
        return false;
}
//TODO:代码验证
bool BUFF::run(cv::Mat src, Robotstatus &robotstatus, RobotCMD &robotcmd) {
    auto time_start = std::chrono::steady_clock::now();
    Eigen::Vector2d angles;
    vector<Fan> fans;

    Eigen::Quaternionf q_raw(robotstatus.q[0], robotstatus.q[1], robotstatus.q[2], robotstatus.q[3]);
    Eigen::Quaternionf q(q_raw.matrix().transpose());
    Eigen::Matrix3d R_IW = q.matrix().cast<double>();
    //TODO：根据实际的速度变化，选择不同的速度策略。(假设它的速度变化幅度不大，可以通过滤波处理)
    coordsolver.getSpeed(28.3);

    auto time_crop = std::chrono::steady_clock::now();
    if (!detector.detect(src,objects,robotstatus)) {
        lost_cnt++;
        is_last_target_exists = false;
        last_target_area = 0;
        return false;
    }
    auto time_infer = std::chrono::steady_clock::now();

    ///------------------------生成扇叶对象----------------------------------------------
    for (auto object: objects) {
        Fan fan;
        fan.id = object.cls;
        fan.color = object.color;
        fan.conf = object.prob;
        if (object.color == 0)
            fan.key = "B" + string(object.cls == 0 ? "Activated" : "Target");
        if (object.color == 1)
            fan.key = "R" + string(object.cls == 0 ? "Activated" : "Target");

        memcpy(fan.apex2d, object.apex, 5 * sizeof(cv::Point2f));
        for (auto &i: fan.apex2d) {
            i += Point2f((float) roi_offset.x, (float) roi_offset.y);
        }

        std::vector<Point2f> points_pic(fan.apex2d, fan.apex2d + 5);
        auto pnp_result = coordsolver.pnp(points_pic, R_IW);

        fan.armor3d_cam = pnp_result.armor_cam;
        fan.armor3d_world = pnp_result.armor_world;
        fan.centerR3d_cam = pnp_result.R_cam;
        fan.centerR3d_world = pnp_result.R_world;

        fan.euler = pnp_result.euler;
        fan.rmat = pnp_result.rmat;

        fans.push_back(fan);
    }

    ///------------------------生成/分配FanTracker----------------------------
    if (trackers.size() != 0) {
        //维护Tracker队列，删除过旧的Tracker
        for (auto iter = trackers.begin(); iter != trackers.end();) {
            //删除元素后迭代器会失效，需先行获取下一元素
            auto next = iter;
            // cout<<(*iter).second.last_timestamp<<"  "<<src.timestamp<<endl;
            if (((int) robotstatus.timestamp - (*iter).last_timestamp) > max_delta_t)
                next = trackers.erase(iter);
            else
                ++next;
            iter = next;
        }
    }

    //TODO:增加防抖
    std::vector<FanTracker> trackers_tmp;
    //为扇叶分配或新建最佳FanTracker
    for (auto fan = fans.begin(); fan != fans.end(); ++fan) {
        if (trackers.size() == 0) {
            FanTracker fan_tracker((*fan), (int) robotstatus.timestamp);
            trackers_tmp.push_back(fan_tracker);
        } else {
            //1e9无实际意义，仅用于以非零初始化
            double min_v = 1e9;
            int min_last_delta_t = 1e9;
            bool is_best_candidate_exist = false;
            std::vector<FanTracker>::iterator best_candidate;
            for (auto iter = trackers.begin(); iter != trackers.end(); iter++) {
                double delta_t;
                Eigen::AngleAxisd angle_axisd;
                double rotate_speed;
                //----------------------------计算角度,求解转速----------------------------
                //若该扇叶完成初始化,且隔一帧时间较短
                if ((*iter).is_initialized && ((int) robotstatus.timestamp - (*iter).prev_timestamp) < max_delta_t) {
                    delta_t = (int) robotstatus.timestamp - (*iter).prev_timestamp;
                    //目前扇叶到上一次扇叶的旋转矩阵
                    auto relative_rmat = (*iter).prev_fan.rmat.transpose() * (*fan).rmat;
                    angle_axisd = Eigen::AngleAxisd(relative_rmat);
                    auto rotate_axis_world = (*fan).rmat * angle_axisd.axis();
                    sign = ((*fan).centerR3d_world.dot(rotate_axis_world) > 0) ? 1 : -1;
                } else {
                    delta_t = (int) robotstatus.timestamp - (*iter).last_timestamp;
                    //目前扇叶到上一次扇叶的旋转矩阵
                    auto relative_rmat = (*iter).last_fan.rmat.transpose() * (*fan).rmat;
                    //TODO:使用点乘判断旋转方向
                    angle_axisd = Eigen::AngleAxisd(relative_rmat);
                    auto rotate_axis_world = (*fan).rmat * angle_axisd.axis();
                    sign = ((*fan).centerR3d_world.dot(rotate_axis_world) > 0) ? 1 : -1;
                }

                rotate_speed = -1 * (angle_axisd.angle()) / delta_t * 1e3;//计算角速度(rad/s)

                if (abs(rotate_speed) <= max_v && abs(rotate_speed) <= min_v &&
                    ((int) robotstatus.timestamp - (*iter).last_timestamp) <= min_last_delta_t) {
                    min_last_delta_t = (int) robotstatus.timestamp - (*iter).last_timestamp;
                    min_v = rotate_speed;
                    best_candidate = iter;
                    is_best_candidate_exist = true;
                }
                // if (fabs(rotate_speed) <= max_v)
                // {
                //     (*iter).update((*fan), src.timestamp);
                //     (*iter).rotate_speed = rotate_speed;
                //     break;
                // }
            }
            if (is_best_candidate_exist) {
                (*best_candidate).update((*fan), (int) robotstatus.timestamp);
                (*best_candidate).rotate_speed = min_v;
            } else {
                FanTracker fan_tracker((*fan), (int) robotstatus.timestamp);
                trackers_tmp.push_back(fan_tracker);
            }
        }
    }
    for (auto new_tracker: trackers_tmp) {
        trackers.push_back(new_tracker);
    }

    ///------------------------检测待激活扇叶是否存在----------------------------
    Fan target;
    bool is_target_exists = chooseTarget(fans, target);

    //若不存在待击打扇叶则返回false
    if (!is_target_exists) {
        lost_cnt++;
        is_last_target_exists = false;
        cout << "[BUFF] No available target fan or Multiple target fan detected!" << endl;
        return false;
    }

    int avail_tracker_cnt = 0;
    double rotate_speed_sum = 0;
    double mean_rotate_speed = 0;
    Eigen::Vector3d r_center_sum = {0, 0, 0};
    Eigen::Vector3d mean_r_center = {0, 0, 0};

    //计算平均转速与平均R字中心坐标
    for (auto tracker: trackers) {
        if (tracker.is_last_fan_exists && tracker.last_timestamp == (int) robotstatus.timestamp) {
            rotate_speed_sum += tracker.rotate_speed;
            r_center_sum += tracker.last_fan.centerR3d_world;
            avail_tracker_cnt++;
        }
    }

    //若不存在可用的扇叶则返回false
    if (avail_tracker_cnt == 0) {
        cout << "NO AVAILABLE TRACKER" << endl;
        return false;
    }
    mean_rotate_speed = rotate_speed_sum / avail_tracker_cnt;
    mean_r_center = r_center_sum / avail_tracker_cnt;
    double theta_offset = 0;


    ///------------------------进行预测----------------------------
    predictor.mode = (int)robotstatus.vision; // 0: Small 1: Big
    if (!predictor.predict(mean_rotate_speed, mean_r_center.norm(), (int) robotstatus.timestamp, theta_offset)) {
        cout << "[BUFF] Predictor is still progressing!" << endl;
        return false;
    }

    ///------------------------计算击打点----------------------------

    //将角度转化至[-PI,PI范围内]
    sign = -sign;
    if (final_sign == 0) {
        puts("1");
        if (sign == 1) {
            left_cnt++;
        } else if (sign == -1) {
            right_cnt++;
        }
        if (right_cnt < left_cnt && left_cnt == 20) {
            final_sign = 1;
        }
        if (right_cnt > left_cnt && right_cnt == 20) {
            final_sign = -1;
        }
    }
    if (final_sign != 0) {
        sign = final_sign;
    }

    theta_offset = rangedAngleRad(theta_offset);
    // 由offset生成欧拉角和旋转矩阵
    Eigen::Vector3d hit_point_world = {sin(theta_offset * sign) * fan_length,
                                       (cos(theta_offset * sign) - 1) * fan_length, 0};
    Eigen::Vector3d hit_point_cam = {0, 0, 0};
    // Eigen::Vector3d euler_rad = target.euler;
    // Pc = R * Pw + T
    hit_point_world = (target.rmat * hit_point_world) + target.armor3d_world;
    hit_point_cam = coordsolver.pw_to_pc(hit_point_world, R_IW);
    auto r_center_cam = coordsolver.pw_to_pc(target.centerR3d_world, R_IW);
    auto center2d_src = coordsolver.pc_to_pu(r_center_cam);
    auto target2d = coordsolver.pc_to_pu(hit_point_cam);
    auto armor2d = coordsolver.pc_to_pu(target.armor3d_cam);
    angles = coordsolver.getAngle(hit_point_cam, R_IW);

    //-----------------判断扇叶是否发生切换-------------------------
    bool is_switched = false;
    auto delta_t = (int) robotstatus.timestamp - last_timestamp;
    auto relative_rmat = last_fan.rmat.transpose() * target.rmat;
    auto angle_axisd = Eigen::AngleAxisd(relative_rmat);
    auto rotate_speed = (angle_axisd.angle()) / delta_t * 1e3;
    if (abs(rotate_speed) > max_v) is_switched = true;

    lost_cnt = 0;
    last_roi_center = center2d_src;
    last_timestamp = (int) robotstatus.timestamp;
    last_fan = target;
    is_last_target_exists = true;

    if (isnan(angles(0, 0)) || isnan(angles(1, 0))) {
//        last_timestamp = time_infer;
//        last_objects = objects;
        cout << "ANGLES ARE ILLEGAL" << endl;
        return false;
    }

    circle(src, center2d_src, 5, Scalar(0, 255, 255), -1);
    circle(src, target2d, 5, Scalar(255, 0, 0), -1);
    circle(src, armor2d, 5, Scalar(0, 0, 255), -1);

//    printf("sign: %d\n", sign);
//    printf("theta_offset: %f\n", theta_offset);
//    printf("dist: %f\n", mean_r_center.norm());
//    printf("mean_rotate_speed: %f\n", mean_rotate_speed);
//    printf("armor_p_w_2: %f %f %f\n", hit_point_world[0], hit_point_world[1], hit_point_world[2]);
//    printf("armor_p_c: %f %f %f\n", hit_point_cam[0], hit_point_cam[1], hit_point_cam[2]);
//    printf("target_c: %f %f %f\n", target.armor3d_cam[0], target.armor3d_cam[1], target.armor3d_cam[2]);
//    printf("target_w: %f %f %f\n", target.armor3d_world[0], target.armor3d_world[1], target.armor3d_world[2]);

}

void BUFF::checkTarget() {
    cnt++;
    if (cnt == 10) {
        now_center = objects[0].center;
        now_center_R = objects[0].center_R;
        return;
    }
    if (cnt == 20 && X(objects[0].center - objects[0].center_R, now_center - now_center_R) > 0) {
        sign = 1;
        return;
    } else if (cnt == 20 && X(objects[0].center - objects[0].center_R, now_center - now_center_R) < 0) {
        sign = -1;
        return;
    }
}

float BUFF::getTargetPolarAngle(cv::Point2f center, cv::Point2f center_R) {
    return (atan2(center.y - center_R.y, center.x - center_R.x));
}

Point2f BUFF::rotate(cv::Point2f target_point, cv::Point2f center, float theta_offset, float get_length) {
    get_length = getDis(target_point.x - center.x, target_point.y - center.y);
    cv::Point2f hit_center_final;
    float hit_center_angle;
    hit_center_angle = (getTargetPolarAngle(target_point, center) + theta_offset * sign);
    hit_center_final = Point2f(cosf(hit_center_angle) * get_length + center.x,
                               sinf(hit_center_angle) * get_length + center.y);
    return hit_center_final;
}

bool BUFF::isSwitch(BuffObject object1, BuffObject object2) {
    auto angle1 = getTargetPolarAngle(object1.center, object1.center_R);
    auto angle2 = getTargetPolarAngle(object2.center, object2.center_R);
    auto delta_angle = (angle1 - angle2) * 180 / PI;
    if (abs(delta_angle) > 30) {
        return true;
    } else {
        return false;
    }
}
