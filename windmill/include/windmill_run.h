#ifndef RUN
#define RUN

#include <cmath>

#include "windmill_detect.h"
#include "windmill_trajectory.h"
#include "windmill_predict.h"
#include "ParticleFilter.h"
#include "fan_tracker.h"


using namespace std;
using namespace cv;

class BUFF {
public:
    BUFF();

    ~BUFF();

    bool run(cv::Mat src,Robotstatus &robotstatus,RobotCMD &robotcmd);

    void checkTarget();

    float getTargetPolarAngle(cv::Point2f center, cv::Point2f center_R);

    Point2f rotate(cv::Point2f target_point, cv::Point2f center, float theta_offset, float get_length);

    bool isSwitch(BuffObject object1, BuffObject object2);

private:
    const string network_path = "../config/buff.xml";
    const string camera_param_path = "../config/config.yaml";

    bool is_last_target_exists;
    int lost_cnt;
    int last_timestamp;
    double last_target_area{};
    double last_bullet_speed;
    Point2i last_roi_center;
    Point2i roi_offset;
    Size2d input_size;
    std::vector<FanTracker> trackers;
    std::vector<BuffObject> objects;
    std::vector<BuffObject> last_objects;
    const int max_lost_cnt = 4;//最大丢失目标帧数
    const int max_v = 4;       //最大旋转速度(rad/s)
    const int max_delta_t = 100; //使用同一预测器的最大时间间隔(ms)
    const double fan_length = 0.7; //大符臂长(R字中心至装甲板中心)
    const double no_crop_thres = 2e-3;      //禁用ROI裁剪的装甲板占图像面积最大面积比值

    std::deque<BuffObject> Final_object_deque;
    const int max_deque_length = 5;

    float last_pitch{};
    float last_yaw{};

    Eigen::Matrix3d R_T;

    Fan last_fan;

    int cnt;
    bool is_Start;
    int sign;
    int right_cnt;
    int left_cnt;
    int final_sign;

    Point2f now_center;
    Point2f now_center_R;

    BuffDetector detector;
    BuffPredictor predictor;
    CoordSolver coordsolver;

    bool chooseTarget(vector<Fan> &fans, Fan &target);

    Point2i cropImageByROI(Mat &img);

    inline double X(Point A, Point B) {
        return A.y * B.x - B.y * A.x;
    }

    template<typename T>
    inline T getDis(T a, T b) {
        return sqrt(a * a + b * b);
    }

    double rangedAngleRad(double &angle) {
        if (fabs(angle) >= CV_PI) {
            angle -= (angle / fabs(angle)) * CV_2PI;
            angle = rangedAngleRad(angle);
        }
        return angle;
    }

};

#endif