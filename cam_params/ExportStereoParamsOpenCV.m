% 该脚本文件用于将由Matlab R2015a标定工具箱（自动提取标定板角点）
% 标定获取的单目相机标定数据转换成能够被OpenCV FileStorage类读取的“.xml”格式文件
% 文件默认保存路径为当前工作目录，可修改路径

% 将标定获取的数据存入IntrinsicMatrix和DistCoeffMatrix两个矩阵中
IntrinsicMatrix = cameraParams.IntrinsicMatrix';
DistCoeffMatrix = zeros(1, 5);
DistCoeffMatrix(1, 1) = cameraParams.RadialDistortion(1);
DistCoeffMatrix(1, 2) = cameraParams.RadialDistortion(2);
DistCoeffMatrix(1, 3) = cameraParams.TangentialDistortion(1);
DistCoeffMatrix(1, 4) = cameraParams.TangentialDistortion(2);
if(cameraParams.NumRadialDistortionCoefficients == 3)
    DistCoeffMatrix(1, 5) = cameraParams.RadialDistortion(3);
end

% 生成根节点“opencv_storage”
xmldoc = com.mathworks.xml.XMLUtils.createDocument('opencv_storage');
xmlroot = xmldoc.getDocumentElement;

%% 在根节点“opencv_storage”下生成二级子节点“Intrinsics”
Intrinsics = xmldoc.createElement('Intrinsics');
xmlroot.appendChild(Intrinsics);
Intrinsics.setAttribute('type_id', 'opencv-matrix');
% 在二级子节点“Intrinsics”下生成三级子节点“rows”
rows = xmldoc.createElement('rows');
rows.appendChild(xmldoc.createTextNode(sprintf('%d', 3)));
Intrinsics.appendChild(rows);
% 在二级子节点“Intrinsics”下生成三级子节点“cols”
cols = xmldoc.createElement('cols');
cols.appendChild(xmldoc.createTextNode(sprintf('%d', 3)));
Intrinsics.appendChild(cols);
% 在二级子节点“Intrinsics”下生成三级子节点“dt”
dt = xmldoc.createElement('dt');
dt.appendChild(xmldoc.createTextNode(sprintf('%s', 'd')));
Intrinsics.appendChild(dt);
% 在二级子节点“Intrinsics”下生成三级子节点“data”
data = xmldoc.createElement('data');
for i = 1 : 3
    for j = 1 : 3
        data.appendChild(xmldoc.createTextNode(sprintf('%f', IntrinsicMatrix(i, j))));
        if((i == 3) && (j == 3))
            break;
        end
        data.appendChild(xmldoc.createTextNode(sprintf('%s', ' ')));    
    end
end
Intrinsics.appendChild(data);

%% 在根节点“opencv_storage”下生成二级子节点“DistortionCoeff”
DistortionCoeff = xmldoc.createElement('DistortionCoeff');
xmlroot.appendChild(DistortionCoeff);
DistortionCoeff.setAttribute('type_id', 'opencv-matrix');
% 在二级子节点“DistortionCoeff”下生成三级子节点“rows”
rows = xmldoc.createElement('rows');
rows.appendChild(xmldoc.createTextNode(sprintf('%d', 5)));
DistortionCoeff.appendChild(rows);
% 在二级子节点“DistortionCoeff”下生成三级子节点“cols”
cols = xmldoc.createElement('cols');
cols.appendChild(xmldoc.createTextNode(sprintf('%d', 1)));
DistortionCoeff.appendChild(cols);
% 在二级子节点“DistortionCoeff”下生成三级子节点“dt”
dt = xmldoc.createElement('dt');
dt.appendChild(xmldoc.createTextNode(sprintf('%s', 'd')));
DistortionCoeff.appendChild(dt);
% 在二级子节点“DistortionCoeff”下生成三级子节点“data”
data = xmldoc.createElement('data');
for k = 1 : 5
        data.appendChild(xmldoc.createTextNode(sprintf('%f', DistCoeffMatrix(1, k))));
        if(k == 5)
            break;
        end
        data.appendChild(xmldoc.createTextNode(sprintf('%s', ' ')));    
end
DistortionCoeff.appendChild(data);

str = strcat('BinoCalibration', '.xml');
xmlwrite(str, xmldoc);
