#ifndef GMASTER_CV_2022_ARMORNEWYOLO_H
#define GMASTER_CV_2022_ARMORNEWYOLO_H

#include <vector>
#include <string>
#include <cmath>
#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <openvino/openvino.hpp>
#include <inference_engine.hpp>

#define NMS_THRESHOLD 0.20f //NMS参数
#define CONF_THRESHOLD 0.70f //置信度参数
#define CONF_REMAIN 0.25 //保留一帧保留的权重比例，如果不保留填写为0
#define IMG_SIZE  416  //推理图像大小，如果不是640 和 416 需要自己在下面添加anchor
#define ANCHOR 3 //anchor 数量
#define CLS_NUM 14 // 种类数量，请在下面的class_names 补充清楚对应的数量
#define DEVICE "CPU" // 设备选择
#define DEBUG //是否开启debug模式，开启以后输出推理过程和帧数
#define VIDEO //是否展示推理视频
//TODO:将MODEL_PATH设置成局部路经且长期存储在config文件夹下
#define MODEL_PATH "/home/zr/C++_inference_openvino_kpt/demo_inference/gmaster_demo_416_yolov7_kpt/gmaster_demo_416_yolov7_kpt.xml"
//TODO:在主函数维护last_conf
float last_conf;

class yolo_kpt {
public:
    yolo_kpt();

    struct Object {
        cv::Rect_<float> rect;
        int label;
        float prob;
//        cv::Point2f point[4];
        std::vector<cv::Point2f>kpt;
    };
    struct Object_result {
        cv::Rect_<float> bbox;
        int label;
        float prob;
        std::vector<cv::Point2f>kpt;
    };

    cv::Mat letter_box(cv::Mat &src, int h, int w, std::vector<float> &padd);

    std::vector<cv::Point2f>
    scale_box_kpt(std::vector<cv::Point2f> points, std::vector<float> &padd, float raw_w, float raw_h, int idx);

    cv::Rect scale_box(cv::Rect box, std::vector<float> &padd, float raw_w, float raw_h);

    void drawPred(int classId, float conf, cv::Rect box, std::vector<cv::Point2f> point, cv::Mat &frame,
                  const std::vector<std::string> &classes);

    static void generate_proposals(int stride, const float *feat, std::vector<Object> &objects);

    std::vector<Object_result> work(cv::Mat src_img);

private:

#ifdef DEBUG
    cv::TickMeter meter;
#endif
    ov::Core core;

    std::shared_ptr<ov::Model> model;

    ov::CompiledModel compiled_model;

    ov::InferRequest infer_request;

    ov::Tensor input_tensor1;

    const std::vector<std::string> class_names = {
            "B1", "B2", "B3", "B4", "B5", "BO", "BS", "R1", "R2", "R3", "R4", "R5", "RO", "RS"
    };

    static float sigmoid(float x) {
        return static_cast<float>(1.f / (1.f + exp(-x)));
    }

};

#endif //GMASTER_CV_2022_ARMORNEWYOLO_H