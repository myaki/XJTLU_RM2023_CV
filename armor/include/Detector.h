#ifndef GMASTER_CV_2022_DETECTOR_H
#define GMASTER_CV_2022_DETECTOR_H

#include <opencv2/opencv.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <inference_engine.hpp>
#include <iostream>
#include <chrono>
#include <cmath>
#include <Checksum.h>

#include "../../armorrun//include/robotnow.h"
#include "../../armorrun/include/robot.h"
#include "../../comm/CommPort.h"
#include "../../windmill/include/windmill_run.h"
#include "../../camera/include/camera/CamWrapperDH.h"

#include "ArmorYOLO.h"

using namespace std;
using namespace InferenceEngine;

struct TxPacket {
    unsigned char cache[packet_size];

    unsigned char &operator[](int p) {
        return cache[p];
    }

    unsigned char operator[](int p) const {
        return cache[p];
    }

    unsigned char *operator&() {
        return cache;
    }

    /*Constructor*/
    TxPacket() {
        memset(cache, 0, sizeof(0));
    }
};

class Detector
{
private:
    typedef struct {
        float prob;
        std::string name;
        cv::Rect2f rect;
    } Object;

    //have
    Detection_package package_get;
    //sol
    robotnow kalman;

    robot robot_run;

    BUFF buff;

    CommPort &comm;
    TxPacket packet;
    Detector::Object final_obj;
    Detector::Object last_obj;

public:

    //get
    Robotstatus robotstatus;
    //send
    RobotCMD robotcmd;

    Detector(CommPort &c);

    ~Detector();

    void send();

    void sendData();

    void run(cv::Mat &);

    void cmd_start();
};

#endif //GMASTER_CV_2022_DETECTOR_H
