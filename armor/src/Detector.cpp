#include "Detector.h"
//TODO:对于FOR_PC的初始化问题
Detector::Detector(CommPort &c) : comm(c)
{
}
Detector::~Detector() = default;
/*!
 * @brief 运行函数设置
 * @param img_src
 * @return void
 */
void Detector::run(cv::Mat &img_src)
{
    comm.Turnstatus(robotstatus);
    cmd_start();
    cout<<robotstatus.timestamp<<endl;
    if (robotstatus.vision == Vision::CLASSIC || robotstatus.vision == Vision::SENTLY)
    {
        bool have = robot_run.run(robotcmd,img_src,robotstatus);
        //bool have = kalman.predict(robotcmd, img_src, robotstatus);
    }
    else if (robotstatus.vision == Vision::WINDSMALL || robotstatus.vision == Vision::WINDBIG)
    {
        bool have = buff.run(img_src,robotstatus,robotcmd);
    }
    else
    {
        cout << "[Error]Vision is wrong" << endl;
        exit(-1);
    }
    send();
}

void Detector::send()
{
    std::thread t(&Detector::sendData, this);
    t.detach();
}
/**
 * @brief 发送数据处理
 * @return void
 */
void Detector::sendData()
{
    packet[0] = 0x5A;
    if (robotcmd.priority == Priority::CORE) packet[1] = 0;
    if (robotcmd.priority == Priority::DANGER) packet[1] = 1;
    packet[2] = robotcmd.target_id;//基本弃用
    union {
        float actual;
        uint8_t raw[4];
    } tx_x{}, tx_y{};
    //右边的数据可以调手动补偿emmmm
    tx_x.actual = robotstatus.pitch+(float)robotcmd.pitch_angle - 0.162f;
    tx_y.actual = robotstatus.yaw+(float)robotcmd.yaw_angle - 0.01f;

    for (int i = 0; i < 4; i++)
    {
        packet[2 + i] = tx_x.raw[i];  // x
        packet[6 + i] = tx_y.raw[i];  // y
    }
    Crc8Append(&packet, 12);
    //printf("  pitch %.2f yaw %.2f id %d cmd %d  time %.3f  \n", robotcmd.pitch_angle, robotcmd.yaw_angle, robotcmd.target_id, packet[1], robotstatus.timestamp);

    comm.Write(&packet, 12, true);
}

void Detector::cmd_start()
{
    robotcmd.priority = Priority::CORE;
    robotcmd.target_id = 255;
    robotcmd.pitch_angle = 0;
    robotcmd.yaw_angle = 0;
    package_get.detection.clear();
}
