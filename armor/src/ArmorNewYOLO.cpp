#include "ArmorNewYOLO.h"

yolo_kpt::yolo_kpt() {
    model = core.read_model(MODEL_PATH);
    std::shared_ptr<ov::Model> model = core.read_model(MODEL_PATH);
#ifdef DEBUG
    std::cout << "finish read model" << std::endl;
#endif
    compiled_model = core.compile_model(model, DEVICE);
#ifdef DEBUG
    std::cout << "finish compile model" << std::endl;
#endif
    auto input_port = compiled_model.input();
#ifdef DEBUG
    std::cout << "finish build inference request" << std::endl;
#endif
    infer_request = compiled_model.create_infer_request();
    input_tensor1 = infer_request.get_input_tensor(0);
#ifdef DEBUG
    std::cout << "finish creat request,now begin infer..." << std::endl;
#endif
}

cv::Mat yolo_kpt::letter_box(cv::Mat &src, int h, int w, std::vector<float> &padd) {
    // 图像预处理 letterbox部分
    int in_w = src.cols;
    int in_h = src.rows;
    int tar_w = w;
    int tar_h = h;
    float r = std::min(float(tar_h) / in_h, float(tar_w) / in_w);
    int inside_w = round(in_w * r);
    int inside_h = round(in_h * r);
    int padd_w = tar_w - inside_w;
    int padd_h = tar_h - inside_h;
    cv::Mat resize_img;
    resize(src, resize_img, cv::Size(inside_w, inside_h));
    padd_w = padd_w / 2;
    padd_h = padd_h / 2;
    padd.push_back(padd_w);
    padd.push_back(padd_h);
    padd.push_back(r);
    int top = int(round(padd_h - 0.1));
    int bottom = int(round(padd_h + 0.1));
    int left = int(round(padd_w - 0.1));
    int right = int(round(padd_w + 0.1));
    copyMakeBorder(resize_img, resize_img, top, bottom, left, right, 0, cv::Scalar(114, 114, 114));
    return resize_img;
}

cv::Rect yolo_kpt::scale_box(cv::Rect box, std::vector<float> &padd,float raw_w, float raw_h) {
    cv::Rect scaled_box;
    scaled_box.width = box.width / padd[2];
    scaled_box.height = box.height / padd[2];
    scaled_box.x = std::max(std::min((float) ((box.x - padd[0]) / padd[2]), (float) (raw_w - 1)), 0.f);
    scaled_box.y = std::max(std::min((float) ((box.y - padd[1]) / padd[2]), (float) (raw_h - 1)), 0.f);
    return scaled_box;
}

std::vector<cv::Point2f>
yolo_kpt::scale_box_kpt(std::vector<cv::Point2f> points, std::vector<float> &padd, float raw_w, float raw_h, int idx) {
    std::vector<cv::Point2f> scaled_points;
    for (int ii = 0; ii < 4; ii++) {
        points[idx * 4 + ii].x = std::max(
                std::min((points[idx * 4 + ii].x - padd[0]) / padd[2], (float) (raw_w - 1)), 0.f);
        points[idx * 4 + ii].y = std::max(
                std::min((points[idx * 4 + ii].y - padd[1]) / padd[2], (float) (raw_h - 1)), 0.f);
        scaled_points.push_back(points[idx * 4 + ii]);

    }
    return scaled_points;
}

void yolo_kpt::drawPred(int classId, float conf, cv::Rect box,
                        std::vector<cv::Point2f> point,
                        cv::Mat &frame,
                        const std::vector<std::string> &classes) {
    float x0 = box.x;
    float y0 = box.y;
    float x1 = box.x + box.width;
    float y1 = box.y + box.height;

    // box为绿色， 四个点的顺序为左上，左下，右下，右上，对应蓝色，白色，红色，绿色四个颜色
    cv::rectangle(frame, cv::Point(x0, y0), cv::Point(x1, y1), cv::Scalar(0, 255, 0), 1);
    cv::circle(frame, point[0], 2, cv::Scalar(255, 0, 0), 2);
    cv::circle(frame, point[1], 2, cv::Scalar(255, 255, 255), 2);
    cv::circle(frame, point[2], 2, cv::Scalar(0, 0, 255), 2);
    cv::circle(frame, point[3], 2, cv::Scalar(0, 255, 0), 2);
    std::string label = cv::format("%.2f", conf);
    if (!classes.empty()) {
        CV_Assert(classId < (int) classes.size());
        label = classes[classId] + ": " + label;
    }
    int baseLine;
    cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.25, 1, &baseLine);
    y0 = std::max(int(y0), labelSize.height);
    cv::rectangle(frame, cv::Point(x0, y0 - round(1.5 * labelSize.height)),
                  cv::Point(x0 + round(2 * labelSize.width), y0 + baseLine), cv::Scalar(0, 255, 0), cv::FILLED);
    cv::putText(frame, label, cv::Point(x0, y0), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(), 1.5);
}

void yolo_kpt::generate_proposals(int stride, const float *feat, std::vector<Object> &objects) {
    // get the results from proposals

    int feat_w = IMG_SIZE / stride;
    int feat_h = IMG_SIZE / stride;
#if IMG_SIZE == 640
    float anchors[18] = { 11,10,  17,13,  24,18,  35,29,  58,44,  81,67,  122,99,  190,115,  251,137 }; // 6 4 0
#elif IMG_SIZE == 416
    float anchors[18] = {7, 6, 11, 9, 16, 12, 22, 19, 39, 30, 57, 43, 74, 64, 121, 73, 159, 92}; // 4 1 6
#endif
    int anchor_group = 0;
    if (stride == 8)
        anchor_group = 0;
    if (stride == 16)
        anchor_group = 1;
    if (stride == 32)
        anchor_group = 2;

    for (int anchor = 0; anchor < ANCHOR; anchor++) {
        for (int i = 0; i < feat_h; i++) {
            for (int j = 0; j < feat_w; j++) {
                float box_prob = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) +
                                      i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                      j * (5 + CLS_NUM + 4 * 3) + 4];
                box_prob = sigmoid(box_prob);

                // 删除置信度低的bbox
                if (box_prob < CONF_THRESHOLD)
                    continue;
                // xi,yi,pi 是每个关键点的xy坐标和置信度
                float x = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                               j * (5 + CLS_NUM + 4 * 3) + 0];
                float y = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                               j * (5 + CLS_NUM + 4 * 3) + 1];
                float w = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                               j * (5 + CLS_NUM + 4 * 3) + 2];
                float h = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                               j * (5 + CLS_NUM + 4 * 3) + 3];

                float x1 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 19];
                float y1 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 20];
                float p1 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 21];
                float x2 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 22];
                float y2 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 23];
                float p2 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 24];
                float x3 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 25];
                float y3 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 26];
                float p3 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 27];
                float x4 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 28];
                float y4 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 29];
                float p4 = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) + i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                j * (5 + CLS_NUM + 4 * 3) + 30];
                double max_prob = 0;
                int idx = 0;
                for (int t = 5; t < CLS_NUM + 5; t++) {
                    double tp = feat[anchor * feat_h * feat_w * (5 + CLS_NUM + 4 * 3) +
                                     i * feat_w * (5 + CLS_NUM + 4 * 3) +
                                     j * (5 + CLS_NUM + 4 * 3) + t];
                    tp = sigmoid(tp);
                    if (tp > max_prob) {
                        max_prob = tp;
                        idx = t;
                    }
                }
                float cof = std::min(box_prob * max_prob + last_conf * CONF_REMAIN, 1.0); // 添加上一帧的保留权重
//                float cof = std::min(box_prob * max_prob + CONF_REMAIN, 1.0); // 不添加上一帧的保留权重
                if (cof < CONF_THRESHOLD)
                    continue;
//                std::cout<<last_conf<<std::endl;
                last_conf = cof;
                x = (sigmoid(x) * 2 - 0.5 + j) * stride;
                y = (sigmoid(y) * 2 - 0.5 + i) * stride;
                w = pow(sigmoid(w) * 2, 2) * anchors[anchor_group * 6 + anchor * 2];
                h = pow(sigmoid(h) * 2, 2) * anchors[anchor_group * 6 + anchor * 2 + 1];
                x1 = j * stride + x1 * anchors[anchor_group * 6 + anchor * 2];
                y1 = i * stride + y1 * anchors[anchor_group * 6 + anchor * 2 + 1];
                x2 = j * stride + x2 * anchors[anchor_group * 6 + anchor * 2];
                y2 = i * stride + y2 * anchors[anchor_group * 6 + anchor * 2 + 1];
                x3 = j * stride + x3 * anchors[anchor_group * 6 + anchor * 2];
                y3 = i * stride + y3 * anchors[anchor_group * 6 + anchor * 2 + 1];
                x4 = j * stride + x4 * anchors[anchor_group * 6 + anchor * 2];
                y4 = i * stride + y4 * anchors[anchor_group * 6 + anchor * 2 + 1];

                float r_x = x - w / 2;
                float r_y = y - h / 2;
                // store the results
                Object obj;
                obj.rect.x = r_x;
                obj.rect.y = r_y;
                obj.rect.width = w;
                obj.rect.height = h;
                obj.label = idx - 5;
                obj.prob = cof;
                obj.kpt.push_back(cv::Point2f(x1, y1));
                obj.kpt.push_back(cv::Point2f(x2, y2));
                obj.kpt.push_back(cv::Point2f(x3, y3));
                obj.kpt.push_back(cv::Point2f(x4, y4));
                objects.push_back(obj);
            }
        }
    }
}

std::vector<yolo_kpt::Object_result> yolo_kpt::work(cv::Mat src_img) {
    // -------- Step 0. Set hyperparameters --------
    int img_h = IMG_SIZE;
    int img_w = IMG_SIZE;
    cv::Mat img;
    std::vector<float> padd;
    cv::Mat boxed = letter_box(src_img, img_h, img_w, padd);
    cv::cvtColor(boxed, img, cv::COLOR_BGR2RGB);
#ifdef DEBUG
    meter.start();
#endif
    auto data1 = input_tensor1.data<float>();
    for (int h = 0; h < img_h; h++) {
        for (int w = 0; w < img_w; w++) {
            for (int c = 0; c < 3; c++) {
                int out_index = c * img_h * img_w + h * img_w + w;
                data1[out_index] = float(img.at<cv::Vec3b>(h, w)[c]) / 255.0f;
            }
        }
    }
    // -------- Step 6. Start inference --------
    infer_request.infer();
    // -------- Step 7. Process output --------
    auto output_tensor_p8 = infer_request.get_output_tensor(0);
    const float *result_p8 = output_tensor_p8.data<const float>();
    auto output_tensor_p16 = infer_request.get_output_tensor(1);
    const float *result_p16 = output_tensor_p16.data<const float>();
    auto output_tensor_p32 = infer_request.get_output_tensor(2);
    const float *result_p32 = output_tensor_p32.data<const float>();
    std::vector<Object> proposals;
    std::vector<Object> objects8;
    std::vector<Object> objects16;
    std::vector<Object> objects32;
    generate_proposals(8, result_p8, objects8);
    proposals.insert(proposals.end(), objects8.begin(), objects8.end());
    generate_proposals(16, result_p16, objects16);
    proposals.insert(proposals.end(), objects16.begin(), objects16.end());
    generate_proposals(32, result_p32, objects32);
    proposals.insert(proposals.end(), objects32.begin(), objects32.end());
    std::vector<int> classIds;
    std::vector<float> confidences;
    std::vector<cv::Rect> boxes;
    std::vector<cv::Point2f> points;
    for (size_t i = 0; i < proposals.size(); i++) {
        classIds.push_back(proposals[i].label);
        confidences.push_back(proposals[i].prob);
        boxes.push_back(proposals[i].rect);
        for (auto ii : proposals[i].kpt)
            points.push_back(ii);
    }
    std::vector<int> picked;
    std::vector<float> picked_useless;
    std::vector<Object_result>object_result;
    cv::dnn::NMSBoxes(boxes, confidences, CONF_THRESHOLD, NMS_THRESHOLD, picked);
    for (size_t i = 0; i < picked.size(); i++) {
        cv::Rect scaled_box = scale_box(boxes[picked[i]], padd, src_img.cols, src_img.rows);
        std::vector<cv::Point2f> scaled_point = scale_box_kpt(points, padd, src_img.cols, src_img.rows, picked[i]);
        Object_result obj;
        obj.bbox = scaled_box;
        obj.kpt = scaled_point;
        obj.label = classIds[picked[i]];
        obj.prob = confidences[picked[i]];
        object_result.push_back(obj);
#ifdef VIDEO
        drawPred(classIds[picked[i]], confidences[picked[i]], scaled_box, scaled_point, src_img,
                 class_names);
#endif
    }
#ifdef DEBUG
    meter.stop();
    printf("Time: %f\n", meter.getTimeMilli());
    meter.reset();
#endif
#ifdef VIDEO
    cv::imshow("Inference frame", src_img);
    cv::waitKey(1);
#endif
    return object_result;
}
