//
// Created by whoismz on 1/2/22.
//

#ifndef GMASTER_WM_NEW_GLOBALPARAMS_H
#define GMASTER_WM_NEW_GLOBALPARAMS_H

#include <Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <ceres/ceres.h>
#include <thread>
#include <chrono>
#include<mutex>
#include<vector>
#include<string>
#include<stdint.h>
#include<iostream>
#include<cstdio>
#include <yaml-cpp/yaml.h>
#include <ctime>
#include <random>

using namespace std;



using Matrix66d = Eigen::Matrix<double, 6, 6>;
using Matrix61d = Eigen::Matrix<double, 6, 1>;
using Matrix16d = Eigen::Matrix<double, 1, 6>;
using Matrix11d = Eigen::Matrix<double, 1, 1>;
using Matrix33d = Eigen::Matrix<double, 3, 3>;
using Matrix31d = Eigen::Matrix<double, 3, 1>;
using Matrix13d = Eigen::Matrix<double, 1, 3>;
using Matrix99d = Eigen::Matrix<double, 9, 9>;
using Matrix91d = Eigen::Matrix<double, 9, 1>;
using Matrix19d = Eigen::Matrix<double, 1, 9>;
using Matrix93d = Eigen::Matrix<double, 9, 3>;
using Matrix39d = Eigen::Matrix<double, 3, 9>;
using Matrix21d = Eigen::Matrix<double, 2, 1>;
using Matrix22d = Eigen::Matrix<double, 2, 2>;
using Matrix12d = Eigen::Matrix<double, 1, 2>;
using Matrix22f = Eigen::Matrix<float, 2, 2>;
using Matrix21f = Eigen::Matrix<float, 2, 1>;
using Matrix12f = Eigen::Matrix<float, 1, 2>;
using Matrix11f = Eigen::Matrix<float, 1, 1>;
using Matrix55d = Eigen::Matrix<double, 5, 5>;
using Matrix51d = Eigen::Matrix<double, 5, 1>;
using Matrix15d = Eigen::Matrix<double, 1, 5>;
using Matrix53d = Eigen::Matrix<double, 5, 3>;
using Matrix35d = Eigen::Matrix<double, 3, 5>;
using Matrix44d = Eigen::Matrix<double, 4, 4>;


//====================armor======================//
//以下四个为装甲板pnp的长度信息
constexpr double armor_big_l = 0.1175;

constexpr double armor_big_w = 0.0625;

constexpr double armor_small_l = 0.069;

constexpr double armor_small_w = 0.0625;
//世界坐标系偏移常数
constexpr double sol_pc_x = 0.0;

constexpr double sol_pc_y = 0.0;

constexpr double sol_pc_z = 0.0;
//发弹延迟常数
constexpr double shoot_delay = 0.01;
//装甲板序列最大常数
constexpr int armor_max_size = 8;
//高置信度阈值
constexpr int armor_high_threshold_conf = 0.82;
//两次预测最大速度
constexpr double max_delta_dist = 0.3;
//装甲板长宽比阈值
constexpr float armor_radio_threshold = 2.8;
//大于该阈值可以认为开启小陀螺
constexpr float anti_spin_judge_large_thres = 2e4;
//小于该值可以认为关闭小陀螺
constexpr float anti_spin_judge_low_thres = 2e3;
//陀螺最大系数
constexpr int anti_spin_max_r_multiple = 4.5;
//对英雄有威胁的距离
constexpr float hero_danger_zone = 0.99;
//最大目标丢失真帧数
constexpr int max_lost_cnt = 6;
//禁用ROI裁剪的装甲板占图像面积最大面积比值
constexpr double no_crop_ratio = 4e-3;
//最大ROI比例，大于此比例ROI大小为网络输入比例
constexpr double full_crop_ratio = 4e-4;
//使用同一预测器的最大时间间隔(ms)
constexpr int max_delta_t = 50;

constexpr double armor_roi_expand_ratio_width = 1;

constexpr double armor_roi_expand_ratio_height = 2;

//you can use this to print something to DEBUG
//TODO:即将放弃在这里的DEBUG
constexpr bool DEBUG = false;
//是否为本地调试
constexpr bool FOR_PC = true;

#define PI 3.141592653589793238

const int INF = 0x7f7f7f7f;

constexpr size_t packet_size = 15;

//=======================Wind=========================//

constexpr double wind_small_l = 0.066;

constexpr double wind_small_w = 0.027;

constexpr double wind_big_l = 0.1125;

constexpr double wind_big_w = 0.027;

enum class OURSELVES {
    GUARD = 0,
    OTHERS = 1,
};

enum class ColorChoose : uint8_t {
    RED = 0,
    BLUE = 1,
};

enum class GameState : uint8_t {
    SHOOT_NEAR_ONLY = 0,
    SHOOT_FAR = 1,
    COMMON = 255,
};

enum class Priority : uint8_t {
    CORE = 0,
    DANGER = 1,
    NONE = 255,
};

enum class ShootMode : uint8_t {
    COMMON = 0,
    DISTANT = 1,
    ANTITOP = 2,
    SWITCH = 4,
    FOLLOW = 8,
    CRUISE = 16,
    EXIST_HERO = 32,

};

enum class Vision : uint8_t {
    WINDSMALL = 0,
    WINDBIG = 1,
    SENTLY = 2,
    CLASSIC = 3,

};
//接收结构体
struct Robotstatus {
    std::array<double, 4> q;
    Vision vision = Vision::CLASSIC;//uint8_t
    GameState gamestate = GameState::COMMON;
    ColorChoose color = ColorChoose::RED;
    uint8_t target_id = 255;
    float robot_speed = 14.;
    //blood
    std::array<uint16_t, 6> enemy;
    int timestamp = 0;
    float pitch;
    float yaw;
    uint8_t lrc = 0;

}__attribute__((packed));
//发送结构体
struct RobotCMD {
    uint8_t start = (unsigned) 's';

    Priority priority; // uint_8
    uint8_t target_id = 255;//uint
    float pitch_angle = 0;
    float yaw_angle = 0;//place
    uint8_t shoot_mode = static_cast<uint8_t>(ShootMode::CRUISE);
    uint8_t lrc = 0;
    uint8_t end = (unsigned) 'e';

}__attribute__((packed));

//detect结构体
struct bbox_t {
    cv::Rect rect;
    cv::Point2f pts[4];
    float confident;
    int color; //0->blue,1->red,2->grey
    int ID;//0 guard 1,2,3,4,5 6 base

    bool operator==(const bbox_t &bbox) const {
        return (rect == bbox.rect) && (pts == bbox.pts) && (confident == bbox.confident) && (color == bbox.color) &&
               (ID == bbox.ID);
    }

    bool operator!=(const bbox_t &bbox) const {
        return (rect != bbox.rect) || (pts != bbox.pts) || (confident != bbox.confident) || (color != bbox.color) ||
               (ID != bbox.ID);
    }

    bool operator>(const bbox_t &bbox) const {
        return rect.area() > bbox.rect.area();
    }

    bool operator<(const bbox_t &bbox) const {
        return rect.area() < bbox.rect.area();
    }
};

//detect包
struct Detection_package {
    std::vector<bbox_t> detection;
    cv::Mat img;
    std::array<double, 4> q;
    double timestamp = 0;
};

constexpr uint8_t ourselves = static_cast<uint8_t>(OURSELVES::OTHERS);

template<typename T>
bool initMatrix(Eigen::MatrixXd &matrix, std::vector<T> &vector) {
    int cnt = 0;
    for (int i = 0; i < matrix.rows(); i++) {
        for (int j = 0; j < matrix.cols(); j++) {
            matrix(i, j) = vector[cnt];
            cnt++;
        }
    }
    return true;
}

#endif //GMASTER_WM_NEW_GLOBALPARAMS_H
